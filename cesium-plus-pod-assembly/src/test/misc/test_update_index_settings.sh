#!/bin/bash

NODE_ADDRESS='http://192.168.0.5:9701';
#NODE_ADDRESS='http://192.168.0.7:9701';

# Setting property to set
SETTING_PROPERTY_NAME='number_of_replicas';
SETTING_PROPERTY_VALUE=2;

# Indices to update
CORE_INDICES="currency document log g1"
USER_INDICES="user page group like history message invitation"
SUBSCRIPTION_INDICES="subscription"
INDICES="${CORE_INDICES} ${USER_INDICES} ${SUBSCRIPTION_INDICES}"

#INDICES="user"
#SETTING_PROPERTY_NAME='max_result_window';
#SETTING_PROPERTY_VALUE=20000;


echo "--- Updating settings 'index.${SETTING_PROPERTY_NAME}=${SETTING_PROPERTY_VALUE}' on node '${NODE_ADDRESS}'... ---"
echo "Targeted indices: ${INDICES}"
echo ""

for INDEX in $INDICES
do
  echo "Updating '${INDEX}/_settings'..."
  ACK=$(curl -s -XPUT "${NODE_ADDRESS}/${INDEX}/_settings" -d "{\"index\": {\"${SETTING_PROPERTY_NAME}\": ${SETTING_PROPERTY_VALUE} }}")
  if [[ $? -ne 0 ]]; then
    echo "ERROR: fail to update '${NODE_ADDRESS}/currency/_settings'"
    exit 1
  fi
  if [[ "${ACK}" != "{\"acknowledged\":true}" ]]; then
    echo "ERROR: fail to update '${NODE_ADDRESS}/currency/_settings': no acknowledge received"
    echo "${ACK}"
    exit 1
  fi
  echo "Updating '${INDEX}/_settings' [OK]"
done

echo "--- Updating settings 'index.${SETTING_PROPERTY_NAME}=${SETTING_PROPERTY_VALUE}' on node '${NODE_ADDRESS}' [OK] ---"
