package org.duniter.elasticsearch.client.model.filter;

import lombok.Builder;
import lombok.Data;
import org.duniter.elasticsearch.client.model.geom.Envelope;

import java.util.Date;

@Data
@Builder
public class MovementFilter {

    public static MovementFilter nullToEmpty(MovementFilter filter) {
        return filter != null ? filter : MovementFilter.builder().build();
    }


    @Builder.Default
    private Date startDate = null;

    @Builder.Default
    private Date endDate = null;

    @Builder.Default
    private String pubkey = null;

    @Builder.Default
    private String[] issuers = null;
    @Builder.Default
    private String[] recipients = null;

    @Builder.Default
    private String[] fields = null;

    @Builder.Default
    private String queryString = null;
}
