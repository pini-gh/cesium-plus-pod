package org.duniter.elasticsearch.client.model.filter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.duniter.elasticsearch.client.model.geom.Envelope;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserProfileFilter {

    public static UserProfileFilter nullToEmpty(UserProfileFilter filter) {
        return filter != null ? filter : UserProfileFilter.builder().build();
    }

    @Builder.Default
    private Date startDate = null;

    @Builder.Default
    private Date endDate = null;

    @Builder.Default
    private Envelope boundingBox = null;

    @Builder.Default
    private String[] fields = null;

    @Builder.Default
    private String queryString = null;

    @Builder.Default
    private String[] issuers = null;
}
