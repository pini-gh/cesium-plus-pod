package org.duniter.elasticsearch.client.model.geom;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Builder
public class Envelope {

    private double minX;
    private double minY;
    private double maxX;
    private double maxY;

    public Envelope(double minX, double minY, double maxX, double maxY) {
        if (minX > maxX || minY > maxY) {
            this.minX = Double.NaN;
            this.minY = Double.NaN;
            this.maxX = Double.NaN;
            this.maxY = Double.NaN;
        } else {
            this.minX = minX;
            this.minY = minY;
            this.maxX = maxX;
            this.maxY = maxY;
        }
    }
}
