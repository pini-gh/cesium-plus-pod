package org.duniter.elasticsearch.client.model.query;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class BoolQuery {

    SearchQuery[] must;
    SearchQuery[] filter;
    SearchQuery[] should;
    SearchQuery[] mustNot;

    Integer minimumShouldMatch;
    Integer boost;

    @JsonGetter("minimum_should_match")
    public Integer getMinimumShouldMatch() {
        return minimumShouldMatch;
    }

    @JsonGetter("must_not")
    public SearchQuery[] getMustNot() {
        return mustNot;
    }
}
