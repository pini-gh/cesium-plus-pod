package org.duniter.elasticsearch.client.model.query;

import com.fasterxml.jackson.annotation.JsonGetter;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ExistsQuery {

    private String field;
}
