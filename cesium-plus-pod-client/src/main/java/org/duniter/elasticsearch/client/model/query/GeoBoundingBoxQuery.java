package org.duniter.elasticsearch.client.model.query;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.duniter.elasticsearch.model.type.GeoPoint;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GeoBoundingBoxQuery {

    private BoundingBox geoPoint;

    @Data
    @Builder
    @AllArgsConstructor
    public static class BoundingBox {

        private GeoPoint topLeft;
        private GeoPoint bottomRight;

        @JsonGetter("top_left")
        public GeoPoint getTopLeft() {
            return topLeft;
        }

        @JsonGetter("bottom_right")
        public GeoPoint getBottomRight() {
            return bottomRight;
        }
    }

}
