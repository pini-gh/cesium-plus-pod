package org.duniter.elasticsearch.client.model.query;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class QueryString {

    private String query;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String[] fields;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String defaultField;

    @JsonGetter("default_field")
    public String getDefaultField() {
        return defaultField;
    }
}
