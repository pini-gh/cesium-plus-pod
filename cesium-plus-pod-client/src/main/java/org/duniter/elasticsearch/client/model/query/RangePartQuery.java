package org.duniter.elasticsearch.client.model.query;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RangePartQuery {
    private Number gte;
    private Number gt;
    private Number lt;
    private Number lte;
}
