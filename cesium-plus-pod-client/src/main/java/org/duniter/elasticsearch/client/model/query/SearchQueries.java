package org.duniter.elasticsearch.client.model.query;

import com.google.common.collect.ImmutableMap;
import org.duniter.elasticsearch.client.model.geom.Envelope;
import org.duniter.elasticsearch.model.type.GeoPoint;

public class SearchQueries {

    public static SearchQuery queryString(String query) {
        return SearchQuery.builder()
            .queryString(QueryString.builder()
                .query(query)
                .build()
            ).build();
    }

    public static SearchQuery geoBoundingBox(Envelope bbox) {
        return SearchQuery.builder()
            .geoBoundingBox(GeoBoundingBoxQuery.builder()
                .geoPoint(GeoBoundingBoxQuery.BoundingBox.builder()
                    .topLeft(GeoPoint.builder()
                        .lat(Math.max(bbox.getMaxY(), bbox.getMinY()))
                        .lon(Math.min(bbox.getMinX(), bbox.getMaxX()))
                        .build())
                    .bottomRight(GeoPoint.builder()
                        .lat(Math.min(bbox.getMaxY(), bbox.getMinY()))
                        .lon(Math.max(bbox.getMinX(), bbox.getMaxX()))
                        .build())
                    .build())
                .build())
            .build();
    }

    public static SearchQuery match(String field, String value) {
        return SearchQuery.builder()
            .match(ImmutableMap.of(field, value))
            .build();
    }

    public static SearchQuery matchPhrase(String field, String value) {
        return SearchQuery.builder()
            .matchPhrase(ImmutableMap.of(field, value))
            .build();
    }

    public static SearchQuery matchPhrasePrefix(String field, String value) {
        return SearchQuery.builder()
            .matchPhrasePrefix(ImmutableMap.of(field, value))
            .build();
    }

    public static SearchQuery term(String field, String term) {
        return SearchQuery.builder()
            .term(ImmutableMap.of(field, term))
            .build();
    }

    public static SearchQuery terms(String field, String... terms) {
        return SearchQuery.builder()
            .terms(ImmutableMap.of(field, terms))
            .build();
    }

    public static SearchQuery exists(String field) {
        return SearchQuery.builder()
            .exists(ExistsQuery.builder().field(field).build())
            .build();
    }

    public static SearchQuery range(String field, RangePartQuery range) {
        return SearchQuery.builder()
            .range(ImmutableMap.of(field, range))
            .build();
    }
}
