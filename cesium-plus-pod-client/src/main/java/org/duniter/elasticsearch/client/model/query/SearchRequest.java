package org.duniter.elasticsearch.client.model.query;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.common.collect.ImmutableMap;
import lombok.Builder;
import lombok.Data;
import org.duniter.core.util.StringUtils;
import org.duniter.elasticsearch.model.SortDirection;

import java.util.Map;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SearchRequest {

    public static class SearchRequestBuilder{
        public SearchRequestBuilder queryString(String queryString) {
            this.query(SearchQuery.builder()
                    .queryString(QueryString.builder()
                        .query(queryString)
                        .build())
                .build());
            return SearchRequestBuilder.this;
        }

        public SearchRequestBuilder sortBy(String field, SortDirection direction) {
            if (StringUtils.isNotBlank(field)) {
                this.sort(ImmutableMap.of(field, direction == null || direction == SortDirection.ASC ? "asc" : "desc"));
            }
            return SearchRequestBuilder.this;
        }
    }

    private SearchQuery query;

    Integer from;
    Integer size;

    String[] source;

    Map<String, String> sort;

    @JsonGetter("_source")
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    public String[] getSource() {
        return source;
    }
}
