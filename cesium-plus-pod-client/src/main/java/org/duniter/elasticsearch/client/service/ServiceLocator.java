package org.duniter.elasticsearch.client.service;

import org.duniter.core.beans.Bean;

import java.io.Closeable;
import java.io.IOException;

public class ServiceLocator extends org.duniter.core.client.service.ServiceLocator implements Closeable {
    /**
     * The shared instance of this ServiceLocator.
     */
    private static ServiceLocator instance = new ServiceLocator();

    /**
     * Gets the shared instance of this Class
     *
     * @return the shared service locator instance.
     */
    public static ServiceLocator instance() {
        return instance;
    }


    org.duniter.core.client.service.ServiceLocator delegate;

    protected ServiceLocator() {
        init();
    }

    public void init() {
        if (delegate == null) {
            delegate = org.duniter.core.client.service.ServiceLocator.instance();
            delegate.init();
        }
    }

    @Override
    public void close() throws IOException {
        if (delegate != null)
            delegate.close();
    }

    public UserProfileService getUserProfileService() {
        return delegate.getBean(UserProfileService.class);
    }

    public UserSettingsService getUserSettingsService() {
        return delegate.getBean(UserSettingsService.class);
    }

    public <S extends Bean> S getBean(Class<S> clazz) {
        if (delegate == null) init();
        return delegate.getBean(clazz);
    }
}
