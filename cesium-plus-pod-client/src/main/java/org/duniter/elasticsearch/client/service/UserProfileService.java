package org.duniter.elasticsearch.client.service;

import org.duniter.core.beans.Service;
import org.duniter.core.client.model.local.Peer;
import org.duniter.core.client.model.local.Wallet;
import org.duniter.elasticsearch.model.Page;
import org.duniter.elasticsearch.client.model.filter.MovementFilter;
import org.duniter.elasticsearch.client.model.filter.UserProfileFilter;
import org.duniter.elasticsearch.model.blockchain.Movement;
import org.duniter.elasticsearch.model.user.UserProfile;

import javax.annotation.Nullable;
import java.util.stream.Stream;

import org.geojson.FeatureCollection;

public interface UserProfileService extends Service {

    Stream<UserProfile> findAllByFilter(Peer peer, UserProfileFilter filter, @Nullable Page page);

    UserProfile save(Peer peer, Wallet wallet, UserProfile userProfile);

    UserProfile update(Peer peer, Wallet wallet, UserProfile userProfile);

    FeatureCollection toGeoJson(Iterable<UserProfile> profiles, String... fields);

    Stream<Movement> findMovements(Peer peer, MovementFilter filter, @Nullable Page page);

    boolean deleteByPubkey(Peer peer, Wallet wallet, String pubkey);

    boolean delete(Peer peer, Wallet wallet, UserProfile userProfile);
}
