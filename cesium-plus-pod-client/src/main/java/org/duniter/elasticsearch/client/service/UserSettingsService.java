package org.duniter.elasticsearch.client.service;

import lombok.NonNull;
import org.duniter.core.beans.Service;
import org.duniter.core.client.model.local.Peer;
import org.duniter.core.client.model.local.Wallet;
import org.duniter.elasticsearch.model.Page;
import org.duniter.elasticsearch.client.model.filter.UserSettingsFilter;
import org.duniter.elasticsearch.model.user.UserSettings;

import javax.annotation.Nullable;
import java.util.Optional;
import java.util.stream.Stream;

public interface UserSettingsService extends Service {

    Stream<UserSettings> findAllByFilter(@NonNull Peer peer,
                                         @NonNull UserSettingsFilter filter,
                                         @Nullable Page page);

    UserSettings save(Peer peer, Wallet wallet, UserSettings settings);

    Optional<UserSettings> findByPubkey(Peer peer, String pubkey);


    boolean deleteByPubkey(Peer peer, Wallet wallet, String pubkey);

    boolean delete(Peer peer, Wallet wallet, UserSettings settings);
}
