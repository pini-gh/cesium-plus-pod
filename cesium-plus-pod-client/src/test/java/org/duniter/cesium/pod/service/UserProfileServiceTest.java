package org.duniter.cesium.pod.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.duniter.cesium.pod.TestResource;
import org.duniter.core.client.model.bma.jackson.JacksonUtils;
import org.duniter.core.exception.TechnicalException;
import org.duniter.core.util.ArrayUtils;
import org.duniter.core.util.Beans;
import org.duniter.core.util.CollectionUtils;
import org.duniter.elasticsearch.model.Page;
import org.duniter.elasticsearch.model.SortDirection;
import org.duniter.elasticsearch.client.model.filter.MovementFilter;
import org.duniter.elasticsearch.client.model.filter.UserProfileFilter;
import org.duniter.elasticsearch.client.model.geom.Envelope;
import org.duniter.elasticsearch.client.service.UserProfileService;
import org.duniter.elasticsearch.model.blockchain.Movement;
import org.duniter.elasticsearch.model.type.Attachment;
import org.duniter.elasticsearch.model.user.UserProfile;
import org.geojson.FeatureCollection;
import org.junit.*;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
public class UserProfileServiceTest extends AbstractServiceTest<UserProfileService>{

    @ClassRule
    public static final TestResource resource = TestResource.create();

    public UserProfileServiceTest(){
        super(UserProfileService.class);
    }

    @Before
    public void setUp() {
        super.setUp();
        peer = getPeer(resource.getFixtures().getDefaultCurrency());
        wallet = getWallet(resource.getFixtures().getDefaultCurrency(),
            null,
            resource.getFixtures().getUserPublicKey(),
            resource.getFixtures().getUserSecretKey()
            );
    }

    @Test
    public void findAllByFilter() {

        // Should match some profiles (bbox France)
        {
            UserProfileFilter filter = UserProfileFilter.builder()
                .boundingBox(Envelope.builder()
                    .minX(-10).maxX(12)
                    .minY(40).maxY(54)
                    .build())
                .queryString("Lavenier")
                .build();

            List<UserProfile> profiles = service.findAllByFilter(peer, filter, null)
                .peek(up -> log.info(" - {}", up.getTitle()))
                .collect(Collectors.toList());

            Assert.assertNotNull(profiles);
            Assert.assertTrue(profiles.size() > 0);
        }

        // Should not match (bbox USA)
        {
            UserProfileFilter filter = UserProfileFilter.builder()
                .boundingBox(Envelope.builder()
                    .minX(-74).minY(40)
                    .maxX(-72).maxY(42)
                    .build())
                .queryString("Lavenier")
                .build();

            List<UserProfile> profiles = service.findAllByFilter(peer, filter, null)
                .peek(up -> log.info(" - {}", up.getTitle()))
                .collect(Collectors.toList());
            Assert.assertNotNull(profiles);
            Assert.assertTrue(profiles.isEmpty());
        }

        // Get only some fields
        {
            UserProfileFilter filter = UserProfileFilter.builder()
                .queryString("Lavenier")
                .fields(new String[]{UserProfile.Fields.TITLE})
                .build();

            List<UserProfile> profiles = service.findAllByFilter(peer, filter, null)
                .peek(up -> log.info(" - {}", up.getTitle()))
                .collect(Collectors.toList());

            Assert.assertNotNull(profiles);
            Assert.assertTrue(profiles.size() > 0);
        }
    }

    @Test
    public void findMovements() {

        MovementFilter filter = MovementFilter.builder()
            .pubkey(resource.getFixtures().getUserPublicKey())
            .build();

        Page page = Page.builder()
            .size(10)
            .build();

        List<Movement> movements = service.findMovements(peer, filter, page)
            .collect(Collectors.toList());
        Assert.assertNotNull(movements);
        Assert.assertTrue(movements.size() > 0);
    }
    @Test
    public void toGeoJson() {

        // Should match some profiles (bbox France)
        {
            UserProfileFilter filter = UserProfileFilter.builder()
                .boundingBox(Envelope.builder()
                    .minX(-10).maxX(12)
                    .minY(40).maxY(54)
                    .build())
                .queryString("Lavenier")
                .build();

            List<UserProfile> profiles = service.findAllByFilter(peer, filter, null)
                .collect(Collectors.toList());

            Assume.assumeNotNull(profiles);
            Assume.assumeTrue(profiles.size() > 0);

            FeatureCollection geoJson = service.toGeoJson(profiles);
            Assert.assertNotNull(geoJson);
            Assert.assertTrue(CollectionUtils.isNotEmpty(geoJson.getFeatures()));

            Beans.getStream(geoJson.getFeatures()).forEach(feature -> {
                Assert.assertNotNull(feature);
                Assert.assertNotNull(feature.getGeometry());
                Assert.assertNotNull(feature.getProperties());
                Assert.assertTrue(feature.getProperties().containsKey(UserProfile.Fields.ISSUER));
                Assert.assertTrue(feature.getProperties().containsKey(UserProfile.Fields.TITLE));
            });

            // Dump to file
            try {
                File output = new File(resource.getResourceDirectory(), "profiles.geojson");
                ObjectMapper objectMapper = JacksonUtils.getThreadObjectMapper();
                objectMapper.writeValue(output, geoJson);
                log.info("GeoJSon serialize into " + output.getAbsolutePath());

                FileUtils.copyFile(output, new File("target", output.getName()));
            }
            catch (IOException e) {
                Assert.fail("Cannot serialize profiles as geoJson: " + e.getMessage());
            }
        }

    }

    @Test
    public void save() {

        // Save
        {
            UserProfile source = UserProfile.builder()
                .title("This is a test profile")
                .build();

            UserProfile savedProfile = service.save(peer, wallet, source);
            Assert.assertNotNull(savedProfile);
            Assert.assertEquals(savedProfile.getIssuer(), wallet.getPubKeyHash());
            Assert.assertEquals(savedProfile.getIssuer(), savedProfile.getId());
        }

        // Update
        {
            UserProfile source = UserProfile.builder()
            .title("Another test profile")
            .build();
            UserProfile savedProfile = service.update(peer, wallet, source);
            Assert.assertNotNull(savedProfile);
            Assert.assertEquals(savedProfile.getIssuer(), wallet.getPubKeyHash());
            Assert.assertEquals(savedProfile.getIssuer(), savedProfile.getId());
        }
    }

    @Test
    public void createProfilesMap() throws IOException {
        long now = System.currentTimeMillis();

        List<UserProfile> profiles = loadAllProfilesWithGeoPoint(
            UserProfile.Fields.ISSUER,
            UserProfile.Fields.TITLE,
            UserProfile.Fields.CITY,
            UserProfile.Fields.GEO_POINT);
        Assert.assertNotNull(profiles);
        Assert.assertFalse(profiles.isEmpty());

        log.info("Loaded {} profiles in {}ms", profiles.size(), System.currentTimeMillis() - now);

        File output = new File("target/profiles-map.geojson");
        dumpToGeoJsonFile(profiles, output, true);
    }

    /* -- protected -- */


    protected List<UserProfile> loadAllProfilesWithGeoPoint(String... fields) {

        long now = System.currentTimeMillis();

        List<UserProfile> result = Lists.newArrayList();
        int latDelta = 10;
        int lonDelta = 20;
        int counter = 0;
        for (int lat = -90; lat < 90; lat += latDelta) {
            for (int lon = -180; lon < 180; lon += lonDelta) {
                counter++;

                if (counter % 10 == 0) log.trace("Loading geo slice #{}...", counter);

                UserProfileFilter filter = UserProfileFilter.builder()
                    .boundingBox(Envelope.builder()
                        .minX(lon).maxX(lon+lonDelta)
                        .minY(lat).maxY(lat+latDelta)
                        .build())
                    .fields(fields)
                    .build();
                List<UserProfile> slice = loadProfiles(filter);
                result.addAll(slice);
            }
        }

        log.debug("Loaded {} slices loaded in {}ms", counter, System.currentTimeMillis() - now);
        return result;
    }

    protected List<UserProfile> loadProfiles(@NonNull UserProfileFilter filter) {
        return loadProfiles(filter, UserProfile.Fields.ISSUER, null);
    }
    protected List<UserProfile> loadProfiles(@NonNull UserProfileFilter filter, String sortBy, SortDirection sortDirection) {

        if (ArrayUtils.isEmpty(filter.getFields())) {
            filter.setFields(
                new String[]{UserProfile.Fields.ISSUER,
                    UserProfile.Fields.TITLE,
                    UserProfile.Fields.DESCRIPTION,
                    UserProfile.Fields.TIME,
                    UserProfile.Fields.ADDRESS,
                    UserProfile.Fields.CITY,
                    UserProfile.Fields.AVATAR + "." + Attachment.JsonFields.CONTENT_TYPE,
                    UserProfile.Fields.GEO_POINT
                });
        }

        List<UserProfile> profiles = Lists.newArrayList();
        Page page = Page.builder()
            .size(1000)
            .sortBy(sortBy)
            .sortDirection(sortDirection)
            .build();

        int pageNumber = 0;
        try {
            boolean hasMorePage;
            do {
                page.setFrom(pageNumber++ * page.getSize());
                List<UserProfile> pageResult = service.findAllByFilter(peer, filter, page)
                    .collect(Collectors.toList());
                int resultCount = CollectionUtils.size(pageResult);
                if (resultCount > 0) profiles.addAll(pageResult);

                hasMorePage = resultCount == page.getSize();
            } while (hasMorePage);
        }
        catch (Exception e) {
            log.error("Error while fetching page #{}: {}", pageNumber, e.getMessage());
            // Continue
        }

        log.trace("{} profiles loaded ({} pages)", profiles.size());

        return profiles;
    }

    protected void dumpToGeoJsonFile(
        @NonNull List<UserProfile> profiles,
        @NonNull File file,
        boolean deleteIfExists) throws IOException {
        ObjectMapper objectMapper = JacksonUtils.getThreadObjectMapper();

        // Write fake profiles into GeoJson file
        if (file.exists()) {
            if (deleteIfExists) FileUtils.delete(file);
            else throw new TechnicalException("File already exists");
        }

        // Keep only profile with a point
        List<UserProfile> geoProfiles = profiles.stream()
            .filter(this::hasGeoPoint)
            .collect(Collectors.toList());

        if (geoProfiles.size() > 0) {
            objectMapper.writeValue(file, service.toGeoJson(geoProfiles));
            log.info("Write {} Geo profiles at {}", geoProfiles.size(), file.getAbsolutePath());
        }
    }

    protected boolean hasGeoPoint(UserProfile p) {
        return p.getGeoPoint() != null && p.getGeoPoint().getLat() != null && p.getGeoPoint().getLon() != null;
    }
}
