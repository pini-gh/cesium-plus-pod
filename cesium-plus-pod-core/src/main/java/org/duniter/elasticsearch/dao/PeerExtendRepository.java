package org.duniter.elasticsearch.dao;

import org.duniter.core.client.model.local.Peer;
import org.duniter.core.client.repositories.PeerRepository;
import org.elasticsearch.index.query.QueryBuilder;

public interface PeerExtendRepository extends PeerRepository, TypeRepository<PeerExtendRepository> {

    String TYPE = "peer";

    Peer create(Peer peer);

    Peer update(Peer peer);

    long countByCurrency(String currency, QueryBuilder query);

    void deleteByCurrencyAndId(String currency, String id);
}