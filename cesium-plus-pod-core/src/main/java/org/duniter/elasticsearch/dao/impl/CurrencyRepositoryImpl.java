package org.duniter.elasticsearch.dao.impl;

/*
 * #%L
 * UCoin Java :: Core Client API
 * %%
 * Copyright (C) 2014 - 2016 EIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.collect.Lists;
import org.duniter.core.client.util.KnownCurrencies;
import org.duniter.core.exception.TechnicalException;
import org.duniter.core.util.Beans;
import org.duniter.core.util.Preconditions;
import org.duniter.core.util.StringUtils;
import org.duniter.elasticsearch.dao.AbstractIndexTypeRepository;
import org.duniter.elasticsearch.dao.CurrencyExtendRepository;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequestBuilder;
import org.elasticsearch.action.index.IndexRequestBuilder;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.update.UpdateRequestBuilder;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.duniter.elasticsearch.model.blockchain.Currency;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by blavenie on 29/12/15.
 */
public class CurrencyRepositoryImpl extends AbstractIndexTypeRepository<CurrencyExtendRepository> implements CurrencyExtendRepository {

    protected static final String REGEXP_WORD_SEPARATOR = "[-\\t@# _]+";

    private static String defaultId;

    public CurrencyRepositoryImpl(){
        super(INDEX, RECORD_TYPE);
    }


    @Override
    public <S extends Currency> Iterable<S> saveAll(Iterable<S> iterable) {
        return Beans.getStream(iterable)
            .map(this::save)
            .collect(Collectors.toList());
    }

    @Override
    public <S extends Currency> S save(S currency) {
        Preconditions.checkNotNull(currency);
        Preconditions.checkNotNull(currency.getId());

        boolean isNew = existsById(currency.getId());
        if (isNew) return (S) create(currency);
        return (S) update(currency);
    }

    @Override
    public Currency create(final Currency currency) {

        try {

            if (currency instanceof Currency) {
                fillTags((Currency)currency);
            }

            // Preparing indexBlocksFromNode
            IndexRequestBuilder request = client.prepareIndex(INDEX, RECORD_TYPE)
                    .setId(currency.getId())
                    .setRefresh(true)
                    .setSource(getObjectMapper().writeValueAsBytes(currency));

            // Execute indexBlocksFromNode
            client.safeExecuteRequest(request, true);

        } catch(JsonProcessingException e) {
            throw new TechnicalException(e);
        }

        return currency;
    }

    @Override
    public Currency update(final Currency currency) {
        try {

            fillTags(currency);

            // Serialize into JSON
            byte[] json = getObjectMapper().writeValueAsBytes(currency);

            UpdateRequestBuilder updateRequest = client.prepareUpdate(INDEX, RECORD_TYPE, currency.getId())
                    .setDoc(json);

            // Execute indexBlocksFromNode
            updateRequest
                    .setRefresh(true)
                    .execute();

        } catch(JsonProcessingException e) {
            throw new TechnicalException(e);
        }


        return currency;
    }

    @Override
    public void updateMemberCount(final String currency, int memberCount) {
        client.prepareUpdate(INDEX, RECORD_TYPE, currency)
                .setDoc(String.format("{\"%s\": %s}", Currency.Fields.MEMBERS_COUNT,
                        memberCount
                ).getBytes()).execute().actionGet();
    }

    @Override
    public void updateDividend(final String currency, long dividend) {
        client.prepareUpdate(INDEX, RECORD_TYPE, currency)
                .setDoc(String.format("{\"%s\": %s, \"%s\": %s}",
                    Currency.Fields.DIVIDEND, dividend,
                    // For compatibility
                    Currency.JsonFields.LAST_UD, dividend
                ).getBytes()).execute().actionGet();
    }

    @Override
    public void delete(final Currency currency) {
        Preconditions.checkNotNull(currency);
        Preconditions.checkArgument(StringUtils.isNotBlank(currency.getId()));

        // Delete the document
        client.prepareDelete(INDEX, RECORD_TYPE, currency.getId()).execute().actionGet();
    }

    @Override
    public void deleteAll(Iterable<? extends Currency> items) {
        Beans.getStream(items)
            .map(Currency::getId)
            .filter(Objects::nonNull)
            .forEach(this::deleteById);
    }

    @Override
    public void deleteAll() {
        findAllIds().forEach(this::deleteById);
    }

    @Override
    public Optional<Currency> findById(String currencyId) {
        Currency result = client.getSourceById(INDEX, RECORD_TYPE, currencyId, Currency.class);
        if (result == null) return Optional.empty();
        result.setId(currencyId);
        return Optional.of(result);
    }

    @Override
    public Iterable<Currency> findAll() {
        SearchRequestBuilder request = client.prepareSearch(INDEX)
                .setTypes(RECORD_TYPE)
                .setSize(pluginSettings.getIndexBulkSize())
                .setFetchSource(true);
        return toList(request.execute().actionGet(), Currency.class);
    }

    @Override
    public Iterable<Currency> findAllById(Iterable<String> ids) {
        return Beans.getStream(ids)
            .map(id -> this.findById(id).orElse(null))
            .filter(Objects::nonNull)
            .collect(Collectors.toList());
    }

    @Override
    public Iterable<String> findAllIds() {
        SearchRequestBuilder request = client.prepareSearch(INDEX)
                .setTypes(RECORD_TYPE)
                .setSize(pluginSettings.getIndexBulkSize())
                .setFetchSource(false);

        return executeAndGetIds(request.execute().actionGet());
    }

    public boolean existsIndex() {
        return client.existsIndex(INDEX);
    }

    @Override
    public XContentBuilder createTypeMapping() {
        try {
            XContentBuilder mapping = XContentFactory.jsonBuilder().startObject()
                    .startObject(RECORD_TYPE)
                    .startObject("properties")

                    // firstBlockSignature
                    .startObject(Currency.Fields.FIRST_BLOCK_SIGNATURE)
                    .field("type", "string")
                    .field("index", "not_analyzed")
                    .endObject()

                    // member count
                    .startObject(Currency.Fields.MEMBERS_COUNT)
                    .field("type", "integer")
                    .endObject()

                    // lastUD - @deprecated
                    .startObject(Currency.JsonFields.LAST_UD)
                    .field("type", "long")
                    .endObject()

                    // Dividend
                    .startObject(Currency.Fields.DIVIDEND)
                    .field("type", "long")
                    .endObject()

                    // unitbase
                    .startObject(Currency.Fields.UNITBASE)
                    .field("type", "integer")
                    .endObject()

                    // tags
                    .startObject(Currency.Fields.TAGS)
                    .field("type", "completion")
                    .field("search_analyzer", "simple")
                    .field("analyzer", "simple")
                    .field("preserve_separators", "false")

                    .endObject()
                    .endObject()
                    .endObject().endObject();

            return mapping;
        }
        catch(IOException ioe) {
            throw new TechnicalException(String.format("Error while getting mapping for index [%s/%s]: %s", INDEX, RECORD_TYPE, ioe.getMessage()), ioe);
        }
    }

    /**
     * Return the default currency
     * @return
     */
    public String getDefaultId() {

        if (defaultId != null) return defaultId;

        boolean enableBlockchainIndexation = pluginSettings.enableBlockchainIndexation() && existsIndex();
        try {
            Iterator<String> ids = enableBlockchainIndexation ? findAllIds().iterator() : null;
            if (ids != null && ids.hasNext()) {
                defaultId = ids.next();
                return defaultId;
            }
        } catch(Throwable t) {
            // Continue (index not read yet?)
        }
        // By default: return G1
        return KnownCurrencies.G1;
    }

    /* -- internal methods -- */

    @Override
    protected void createIndex() throws JsonProcessingException {
        logger.info(String.format("Creating index [%s]", INDEX));

        CreateIndexRequestBuilder createIndexRequestBuilder = client.admin().indices().prepareCreate(INDEX);
        org.elasticsearch.common.settings.Settings indexSettings = org.elasticsearch.common.settings.Settings.settingsBuilder()
                .put("number_of_shards", 3)
                .put("number_of_replicas", 1)
                .put("analyzer", pluginSettings.getDefaultStringAnalyzer())
                .build();
        createIndexRequestBuilder.setSettings(indexSettings);
        createIndexRequestBuilder.addMapping(RECORD_TYPE, createTypeMapping());
        createIndexRequestBuilder.execute().actionGet();
    }

    protected void fillTags(Currency currency) {
        String currencyName = currency.getId();
        String[] tags = currencyName.split(REGEXP_WORD_SEPARATOR);
        List<String> tagsList = Lists.newArrayList(tags);

        // Convert as a sentence (replace separator with a space)
        String sentence = currencyName.replaceAll(REGEXP_WORD_SEPARATOR, " ");
        if (!tagsList.contains(sentence)) {
            tagsList.add(sentence);
        }

        currency.setTags(tagsList.toArray(new String[tagsList.size()]));
    }

}
