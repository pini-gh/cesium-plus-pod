package org.duniter.elasticsearch.dao.impl;

/*
 * #%L
 * UCoin Java :: Core Client API
 * %%
 * Copyright (C) 2014 - 2016 EIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.duniter.core.client.model.bma.EndpointApi;
import org.duniter.core.client.model.bma.NetworkWs2pHeads;
import org.duniter.core.client.model.local.Peer;
import org.duniter.core.client.model.local.Peers;
import org.duniter.core.exception.TechnicalException;
import org.duniter.core.util.Beans;
import org.duniter.core.util.StringUtils;
import org.duniter.elasticsearch.dao.AbstractRepository;
import org.duniter.elasticsearch.dao.PeerExtendRepository;
import org.elasticsearch.action.bulk.BulkRequestBuilder;
import org.elasticsearch.action.index.IndexRequestBuilder;
import org.elasticsearch.action.search.SearchPhaseExecutionException;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.action.update.UpdateRequestBuilder;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.NestedQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.SingleBucketAggregation;
import org.elasticsearch.search.aggregations.metrics.max.Max;

import java.io.IOException;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by blavenie on 29/12/15.
 */
public class PeerRepositoryImpl extends AbstractRepository implements PeerExtendRepository {

    public PeerRepositoryImpl(){
        super("duniter.dao.peer");
    }

    @Override
    public String getType() {
        return TYPE;
    }

    @Override
    public <S extends Peer> Iterable<S> saveAll(Iterable<S> peers) {
        return Beans.getStream(peers)
            .map(this::save)
            .collect(Collectors.toList());
    }

    public <S extends Peer> S save(S peer) {
        org.duniter.core.util.Preconditions.checkNotNull(peer);
        org.duniter.core.util.Preconditions.checkNotNull(peer.getId());
        org.duniter.core.util.Preconditions.checkNotNull(peer.getCurrency());

        boolean isNew = existsByCurrencyAndId(peer.getCurrency(), peer.getId());
        if (isNew) return (S) create(peer);
        return (S) update(peer);
    }

    @Override
    public Peer create(Peer peer) {
        Preconditions.checkNotNull(peer);
        Preconditions.checkArgument(StringUtils.isNotBlank(peer.getId()));
        Preconditions.checkArgument(StringUtils.isNotBlank(peer.getCurrency()));
        //Preconditions.checkNotNull(peer.getHash());
        Preconditions.checkNotNull(peer.getHost());
        Preconditions.checkNotNull(peer.getApi());

        // Serialize into JSON
        // WARN: must use GSON, to have same JSON result (e.g identities and joiners field must be converted into String)
        try {
            String json = getObjectMapper().writeValueAsString(peer);

            // Preparing indexBlocksFromNode
            IndexRequestBuilder indexRequest = client.prepareIndex(peer.getCurrency(), TYPE)
                    .setId(peer.getId())
                    .setSource(json);

            // Execute indexBlocksFromNode
            indexRequest
                    .setRefresh(true)
                    .execute();
        }
        catch(JsonProcessingException e) {
            throw new TechnicalException(e);
        }
        return peer;
    }

    @Override
    public Peer update(Peer peer) {
        Preconditions.checkNotNull(peer);
        Preconditions.checkArgument(StringUtils.isNotBlank(peer.getId()));
        Preconditions.checkArgument(StringUtils.isNotBlank(peer.getCurrency()));
        //Preconditions.checkNotNull(peer.getHash());
        Preconditions.checkNotNull(peer.getHost());
        Preconditions.checkNotNull(peer.getApi());

        // Serialize into JSON
        try {
            String json = getObjectMapper().writeValueAsString(peer);

            // Preparing indexBlocksFromNode
            UpdateRequestBuilder updateRequest = client.prepareUpdate(peer.getCurrency(), TYPE, peer.getId())
                    .setDoc(json);

            // Execute indexBlocksFromNode
            updateRequest
                    .setRefresh(true)
                    .execute();
        }
        catch(JsonProcessingException e) {
            throw new TechnicalException(e);
        }
        return peer;
    }

    @Override
    public Iterable<Peer> findAllById(Iterable<String> ids) {
        return Beans.getStream(ids)
            .map(id -> this.findById(id).orElse(null))
            .filter(Objects::nonNull)
            .collect(Collectors.toList());
    }


    @Override
    public long countByCurrency(String currency, QueryBuilder query) {
        // Prepare count request
        SearchRequestBuilder searchRequest = client
            .prepareSearch(currency)
            .setTypes(getType())
            .setFetchSource(false)
            .setSearchType(SearchType.QUERY_AND_FETCH)
            .setSize(0);

        // Query
        if (query != null) {
            searchRequest.setQuery(query);
        }

        // Execute query
        try {
            SearchResponse response = searchRequest.execute().actionGet();
            return response.getHits().getTotalHits();
        }
        catch(SearchPhaseExecutionException e) {
            // Failed or no item on index
            logger.error(String.format("Error while counting comment replies: %s", e.getMessage()), e);
        }
        return 1;
    }

    @Override
    public void deleteAll(Iterable<? extends Peer> items) {
        Beans.getStream(items)
            .forEach(this::delete);
    }


    @Override
    public void delete(Peer peer) {
        Preconditions.checkNotNull(peer);
        deleteByCurrencyAndId(peer.getCurrency(), peer.getId());
    }

    @Override
    public void deleteByCurrencyAndId(String currency, String id) {
        Preconditions.checkArgument(StringUtils.isNotBlank(currency));
        Preconditions.checkArgument(StringUtils.isNotBlank(id));

        // Delete the document
        client.prepareDelete(currency, TYPE, id).execute().actionGet();
    }

    @Override
    public List<Peer> getPeersByCurrencyId(String currencyId) {
        // Loading all peers in memory may be unsafe !
        // Applying workaround: return only the Duniter peer defined in config.
        return ImmutableList.of(pluginSettings.checkAndGetDuniterPeer());
    }

    @Override
    public List<Peer> getPeersByCurrencyIdAndApi(String currencyId, String endpointApi) {
        return getPeersByCurrencyIdAndApiAndPubkeys(currencyId, endpointApi, null);
    }

    @Override
    public List<Peer> getPeersByCurrencyIdAndApiAndPubkeys(String currencyId, String endpointApi, String[] pubkeys) {
        Preconditions.checkNotNull(currencyId);
        Preconditions.checkNotNull(endpointApi);

        SearchRequestBuilder request = client.prepareSearch(currencyId)
                .setTypes(TYPE)
                .setSize(1000);

        // Query = filter on status UP
        NestedQueryBuilder statusQuery = QueryBuilders.nestedQuery(Peer.Fields.STATS,
                QueryBuilders.boolQuery()
                        .filter(QueryBuilders.termQuery(Peer.Fields.STATS + "." + Peer.Stats.Fields.STATUS, Peer.PeerStatus.UP.name())));

        BoolQueryBuilder boolQuery = QueryBuilders.boolQuery()
                .filter(QueryBuilders.termQuery(Peer.Fields.API, endpointApi));
        if (ArrayUtils.isNotEmpty(pubkeys)) {
            boolQuery.filter(QueryBuilders.termsQuery(Peer.Fields.PUBKEY, pubkeys));
        }
        boolQuery.must(statusQuery);

        request.setQuery(QueryBuilders.constantScoreQuery(boolQuery));

        return toPeers(request);
    }

    @Override
    public List<Peer> getUpPeersByCurrencyId(String currencyId, String[] includePubkeys) {
        Preconditions.checkNotNull(currencyId);

        SearchRequestBuilder request = client.prepareSearch(currencyId)
                .setTypes(TYPE)
                .setSize(pluginSettings.getIndexBulkSize());

        BoolQueryBuilder query = QueryBuilders.boolQuery();

        // Query = filter on UP status
        NestedQueryBuilder statusQuery = QueryBuilders.nestedQuery(Peer.Fields.STATS,
                QueryBuilders.boolQuery()
                        .filter(QueryBuilders.termQuery(Peer.Fields.STATS + "." + Peer.Stats.Fields.STATUS, Peer.PeerStatus.UP.name())));
        query.must(statusQuery);

        // Filter on pubkeys
        if (ArrayUtils.isNotEmpty(includePubkeys)) {
            BoolQueryBuilder pubkeysQuery = QueryBuilders.boolQuery();
            pubkeysQuery.filter(QueryBuilders.termsQuery(Peer.Fields.PUBKEY, includePubkeys));
            query.must(pubkeysQuery);
        }

        request.setQuery(QueryBuilders.constantScoreQuery(query));

        return toPeers(request);
    }

    @Override
    public List<NetworkWs2pHeads.Head> getWs2pPeersByCurrencyId(String currencyId, String[] pubkeys) {
        Preconditions.checkNotNull(currencyId);

        SearchRequestBuilder request = client.prepareSearch(currencyId)
                .setTypes(TYPE)
                .setSize(1000);

        BoolQueryBuilder query = QueryBuilders.boolQuery();

        // Query = filter on UP status
        NestedQueryBuilder statusQuery = QueryBuilders.nestedQuery(Peer.Fields.STATS,
                QueryBuilders.boolQuery()
                        .filter(QueryBuilders.termQuery(Peer.Fields.STATS + "." + Peer.Stats.Fields.STATUS, Peer.PeerStatus.UP.name())));
        query.must(statusQuery);

        // Filter on pubkeys
        if (ArrayUtils.isNotEmpty(pubkeys)) {
            BoolQueryBuilder pubkeysQuery = QueryBuilders.boolQuery();
            pubkeysQuery.filter(QueryBuilders.termsQuery(Peer.Fields.PUBKEY, pubkeys));
            query.must(pubkeysQuery);
        }

        // Filter on WS2P api
        if (ArrayUtils.isNotEmpty(pubkeys)) {
            BoolQueryBuilder apiQuery = QueryBuilders.boolQuery();
            apiQuery.filter(QueryBuilders.termsQuery(Peer.Fields.API, EndpointApi.WS2P.name()));
            query.must(apiQuery);
        }

        request.setQuery(QueryBuilders.constantScoreQuery(query));

        return toPeers(request).stream()
                .map(Peers::toWs2pHead)
                // Skip if no message
                .filter(head -> head.getMessage() != null)
                .collect(Collectors.toList());
    }

    @Override
    public boolean existsByCurrencyAndId(String currency, String id) {
        return client.isDocumentExists(currency, TYPE, id);
    }

    @Override
    public Long getMaxLastUpTime(String currencyName) {

        // Prepare request
        SearchRequestBuilder searchRequest = client
                .prepareSearch(currencyName)
                .setTypes(TYPE)
                .setFetchSource(false)
                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH);

        // Get max(number)
        searchRequest.addAggregation(AggregationBuilders.nested(Peer.Fields.STATS)
                .path(Peer.Fields.STATS)
                .subAggregation(
                        AggregationBuilders.max(Peer.Stats.Fields.LAST_UP_TIME)
                            .field(Peer.Fields.STATS + "." + Peer.Stats.Fields.LAST_UP_TIME)
                            .missing(0)
                ));

        // Execute query
        SearchResponse searchResponse = searchRequest.execute().actionGet();

        // Read query result
        SingleBucketAggregation stats = searchResponse.getAggregations().get(Peer.Fields.STATS);
        if (stats == null) return null;

        Max result = stats.getAggregations().get(Peer.Stats.Fields.LAST_UP_TIME);
        if (result == null) {
            return null;
        }

        return (result.getValue() == Double.NEGATIVE_INFINITY)
                ? null
                : (long)result.getValue();
    }

    @Override
    public void updatePeersAsDown(String currencyName, long minUpTimeInMs, Collection<String> endpointApis) {

        long minUpTimeInSec = Math.round(minUpTimeInMs / 1000L);
        long firstDownTime = Instant.now().getEpochSecond();

        if (logger.isDebugEnabled()) {
            logger.debug(String.format("[%s] %s Mark peers as DOWN when {last up time < %s}...", currencyName, endpointApis, new Date(minUpTimeInMs)));
        }

        SearchRequestBuilder searchRequest = client.prepareSearch(currencyName)
                .setFetchSource(false)
                .setTypes(TYPE);

        // Query = filter on lastUpTime
        BoolQueryBuilder query = QueryBuilders.boolQuery();

        if (CollectionUtils.isNotEmpty(endpointApis)) {
            query.filter(QueryBuilders.termsQuery(Peer.Fields.API, endpointApis));
        }

        // filter on stats
        NestedQueryBuilder statsQuery = QueryBuilders.nestedQuery(Peer.Fields.STATS,
                QueryBuilders.boolQuery()
                        // lastUpTime < upTimeLimit
                    .filter(QueryBuilders.rangeQuery(Peer.Fields.STATS + "." + Peer.Stats.Fields.LAST_UP_TIME).lt(minUpTimeInSec))
                        // status = UP
                    .filter(QueryBuilders.termQuery(Peer.Fields.STATS + "." + Peer.Stats.Fields.STATUS, Peer.PeerStatus.UP.name())));
        query.must(statsQuery);

        searchRequest.setQuery(QueryBuilders.constantScoreQuery(query));

        BulkRequestBuilder bulkRequest = client.prepareBulk();


        // Execute query, while there is some data
        try {

            int counter = 0;
            boolean loop = true;
            int bulkSize = pluginSettings.getIndexBulkSize();
            searchRequest.setSize(bulkSize);
            SearchResponse response = searchRequest.execute().actionGet();

            // Execute query, while there is some data
            do {

                // Read response
                SearchHit[] searchHits = response.getHits().getHits();
                for (SearchHit searchHit : searchHits) {

                    // Add update operation to bulk
                    bulkRequest.add(
                            client.prepareUpdate(currencyName, TYPE, searchHit.getId())
                            .setDoc(String.format("{\"%s\": {\"%s\": \"%s\", \"%s\": %s}}", Peer.Fields.STATS,
                                    Peer.Stats.Fields.STATUS, Peer.PeerStatus.DOWN.name(),
                                    Peer.Stats.Fields.FIRST_DOWN_TIME, firstDownTime
                            ).getBytes())
                    );
                    counter++;

                    // Flush the bulk if not empty
                    if ((bulkRequest.numberOfActions() % bulkSize) == 0) {
                        client.flushBulk(bulkRequest);
                        bulkRequest = client.prepareBulk();
                    }
                }

                // Prepare next iteration
                if (counter == 0 || counter >= response.getHits().getTotalHits()) {
                    loop = false;
                }
                // Prepare next iteration
                else {
                    searchRequest.setFrom(counter);
                    response = searchRequest.execute().actionGet();
                }
            } while(loop);

            // last flush
            if ((bulkRequest.numberOfActions() % bulkSize) != 0) {
                client.flushBulk(bulkRequest);
            }

            if (counter > 0) {
                logger.info(String.format("[%s] %s peers DOWN", currencyName, counter));
            }

        } catch (SearchPhaseExecutionException e) {
            // Failed or no item on index
            logger.error(String.format("Error while update peer status to DOWN: %s.", e.getMessage()), e);
        }


    }

    @Override
    public boolean hasPeersUpWithApi(String currencyId, Set<String> api) {
        SearchRequestBuilder searchRequest = client.prepareSearch(currencyId)
                .setFetchSource(false)
                .setTypes(TYPE)
                .setSize(0);

        // Query = filter on lastUpTime
        BoolQueryBuilder query = QueryBuilders.boolQuery();

        if (CollectionUtils.isNotEmpty(api)) {
            query.minimumNumberShouldMatch(api.size());
            api.forEach(a -> query.should(QueryBuilders.termQuery(Peer.Fields.API, a)));
        }

        query.must(QueryBuilders.nestedQuery(Peer.Fields.STATS, QueryBuilders.constantScoreQuery(QueryBuilders.boolQuery()
                .filter(QueryBuilders.termQuery(Peer.Fields.STATS + "." + Peer.Stats.Fields.STATUS, Peer.PeerStatus.UP.name())))));

        searchRequest.setQuery(query);
        SearchResponse response = searchRequest.execute().actionGet();
        return response.getHits() != null && response.getHits().getTotalHits() > 0;
    }

    @Override
    public XContentBuilder createTypeMapping() {
        try {
            XContentBuilder mapping = XContentFactory.jsonBuilder()
                    .startObject()
                    .startObject(TYPE);

            startTypeMappingProperties(mapping);

            mapping.endObject().endObject();

            return mapping;
        }
        catch(IOException ioe) {
            throw new TechnicalException("Error while getting mapping for peer index: " + ioe.getMessage(), ioe);
        }
    }

    protected XContentBuilder startTypeMappingProperties(XContentBuilder mapping) {

        try {
            mapping
                    .startObject("properties")

                    // currency
                    .startObject(Peer.Fields.CURRENCY)
                    .field("type", "string")
                    .endObject()

                    // pubkey
                    .startObject(Peer.Fields.PUBKEY)
                    .field("type", "string")
                    .field("index", "not_analyzed")
                    .endObject()

                    // api
                    .startObject(Peer.Fields.API)
                    .field("type", "string")
                    .field("index", "not_analyzed")
                    .endObject()

                    // dns
                    .startObject(Peer.Fields.DNS)
                    .field("type", "string")
                    .endObject()

                    // ipv4
                    .startObject(Peer.Fields.IPV4)
                    .field("type", "string")
                    .endObject()

                    // ipv6
                    .startObject(Peer.Fields.IPV6)
                    .field("type", "string")
                    .endObject()

                    // epId
                    .startObject(Peer.Fields.EP_ID)
                    .field("type", "string")
                    .field("index", "not_analyzed")
                    .endObject()

                    // stats
                    .startObject(Peer.Fields.STATS)
                    .field("type", "nested")
                    //.field("dynamic", "false")
                    .startObject("properties")

                    // stats.software
                    .startObject(Peer.Stats.Fields.SOFTWARE)
                    .field("type", "string")
                    .endObject()

                    // stats.version
                    .startObject(Peer.Stats.Fields.VERSION)
                    .field("type", "string")
                    .endObject()

                    // stats.status
                    .startObject(Peer.Stats.Fields.STATUS)
                    .field("type", "string")
                    .field("index", "not_analyzed")
                    .endObject()

                    // stats.blockNumber
                    .startObject("blockNumber")
                    .field("type", "integer")
                    .endObject()

                    // stats.blockHash
                    .startObject("blockHash")
                    .field("type", "string")
                    .field("index", "not_analyzed")
                    .endObject()

                    // stats.error
                    .startObject("error")
                    .field("type", "string")
                    .endObject()

                    // stats.medianTime
                    .startObject("medianTime")
                    .field("type", "integer")
                    .endObject()

                    // stats.hardshipLevel
                    .startObject("hardshipLevel")
                    .field("type", "integer")
                    .endObject()

                    // stats.consensusPct
                    .startObject("consensusPct")
                    .field("type", "integer")
                    .endObject()

                    // stats.uid
                    .startObject(Peer.Stats.Fields.UID)
                    .field("type", "string")
                    .endObject()

                    // stats.mainConsensus
                    .startObject("mainConsensus")
                    .field("type", "boolean")
                    .field("index", "not_analyzed")
                    .endObject()

                    // stats.forkConsensus
                    .startObject("forkConsensus")
                    .field("type", "boolean")
                    .field("index", "not_analyzed")
                    .endObject()

                    // stats.lastUpTime
                    .startObject(Peer.Stats.Fields.LAST_UP_TIME)
                    .field("type", "integer")
                    .endObject()

                    // stats.firstDownTime
                    .startObject(Peer.Stats.Fields.FIRST_DOWN_TIME)
                    .field("type", "integer")
                    .endObject()


                    .endObject()
                    .endObject()

                    // peering
                    .startObject(Peer.Fields.PEERING)
                    .field("type", "nested")
                    //.field("dynamic", "false")
                    .startObject("properties")

                    // peering.version
                    .startObject(Peer.Peering.Fields.VERSION)
                    .field("type", "string")
                    .endObject()

                    // peering.blockNumber
                    .startObject(Peer.Peering.Fields.BLOCK_NUMBER)
                    .field("type", "integer")
                    .endObject()

                    // peering.blockHash
                    .startObject(Peer.Peering.Fields.BLOCK_HASH)
                    .field("type", "string")
                    .field("index", "not_analyzed")
                    .endObject()

                    // peering.signature
                    .startObject(Peer.Peering.Fields.SIGNATURE)
                    .field("type", "string")
                    .field("index", "not_analyzed")
                    .endObject()

                    // peering.raw
                    .startObject(Peer.Peering.Fields.RAW)
                    .field("type", "string")
                    .field("index", "not_analyzed")
                    .endObject()

                .endObject()
                .endObject()

            .endObject();

            return mapping;
        }
        catch(IOException ioe) {
            throw new TechnicalException("Error while getting mapping for peer index: " + ioe.getMessage(), ioe);
        }
    }

    protected List<Peer> toPeers(SearchRequestBuilder request) {
        List<Peer> result =  toList(request, Peer.class);

        // Make sure to set all peers hash (and id)
        result.forEach(peer -> {
            if (peer.getHash() == null) {
                String hash = cryptoService.hash(peer.toString());
                peer.setHash(hash);
            }
            peer.setId(peer.getHash());
        });

        return result;
    }


    @Override
    public Optional<Peer> findById(String id) {
        throw new TechnicalException("Not implemented. Need the currency");
    }

    @Override
    public boolean existsById(String docId) {
        throw new TechnicalException("Not implemented. Need the currency");
    }

    @Override
    public void deleteById(String s) {
        throw new TechnicalException("Not implemented. Need the currency");
    }

    @Override
    public Iterable<Peer> findAll() {
        throw new TechnicalException("Not implemented. Need the currency");
    }


    @Override
    public void deleteAll() {
        throw new TechnicalException("Not implemented. Need the currency");
    }

    @Override
    public long count() {
        throw new TechnicalException("Not implemented. Need the currency");
    }

    protected List<Peer> toPeers(SearchResponse response) {
        List<Peer> result =  toList(response, Peer.class);

        // Make sure to set all peers hash (and id)
        result.forEach(peer -> {
            if (peer.getHash() == null) {
                String hash = cryptoService.hash(peer.toString());
                peer.setHash(hash);
            }
            peer.setId(peer.getHash());
        });

        return result;
    }
}
