package org.duniter.elasticsearch.rest.security;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.duniter.core.util.Preconditions;
import org.duniter.core.util.StringUtils;

import java.util.concurrent.TimeUnit;

public class QuotaMap {

    public interface MaxExceededListener {
        void onMaxExceeded(String key);
    }

    public interface RemovableListener extends com.google.common.cache.RemovalListener<String, Integer> {

    }

    private final String name;
    private final int maxCallCount;
    private final Cache<String, Integer> counterMap;

    private final MaxExceededListener exceedListener;

    public QuotaMap(
            String name,
            int max, int duration, TimeUnit unit,
            MaxExceededListener exceedListener,
            RemovableListener removalListener) {
        this.name = name;
        this.maxCallCount = max;
        this.exceedListener = exceedListener;
        CacheBuilder builder = CacheBuilder.newBuilder()
                .expireAfterWrite(duration, unit)
                .concurrencyLevel(4);
        if (removalListener != null) builder.removalListener(removalListener);

        counterMap = builder.build();
    }

    boolean increment(String key) {
        Preconditions.checkArgument(StringUtils.isNotEmpty(key));

        Integer counter = counterMap.getIfPresent(key);
        if (counter == null) {
            counter = 0;
        }
        else if (counter == -1) {
            return false; // Limit already exceed: quit
        }

        counter += 1;

        // First exceed the limit
        if (counter > maxCallCount) {

            // Remember max has been exceeded
            counterMap.put(key, -1);

            if (exceedListener != null) exceedListener.onMaxExceeded(key);
            return false;
        }
        else {

            // Store the counter
            counterMap.put(key, counter);

            return true;
        }
    }
}
