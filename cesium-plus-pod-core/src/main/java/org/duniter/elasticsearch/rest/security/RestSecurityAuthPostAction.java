package org.duniter.elasticsearch.rest.security;

/*
 * #%L
 * duniter4j-elasticsearch-plugin
 * %%
 * Copyright (C) 2014 - 2016 EIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.duniter.core.service.CryptoService;
import org.duniter.core.util.StringUtils;
import org.duniter.elasticsearch.PluginSettings;
import org.duniter.elasticsearch.security.challenge.AuthToken;
import org.duniter.elasticsearch.security.challenge.ChallengeMessageStore;
import org.duniter.elasticsearch.security.token.SecurityTokenStore;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.inject.Inject;
import org.elasticsearch.common.logging.ESLogger;
import org.elasticsearch.common.logging.ESLoggerFactory;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.rest.*;

import java.util.Objects;

import static org.elasticsearch.rest.RestRequest.Method.POST;
import static org.elasticsearch.rest.RestStatus.FORBIDDEN;
import static org.elasticsearch.rest.RestStatus.OK;

public class RestSecurityAuthPostAction extends BaseRestHandler {

    private static final ESLogger log = ESLoggerFactory.getLogger(RestSecurityAuthPostAction.class.getName());

    private ChallengeMessageStore challengeMessageStore;
    private SecurityTokenStore securityTokenStore;
    private ObjectMapper objectMapper;

    private final PluginSettings pluginSettings;

    @Inject
    public RestSecurityAuthPostAction(Settings settings, RestController controller, Client client,
                                      RestSecurityController securityController,
                                      ChallengeMessageStore challengeMessageStore,
                                      SecurityTokenStore securityTokenStore,
                                      CryptoService cryptoService,
                                      PluginSettings pluginSettings) {
        super(settings, controller, client);
        this.challengeMessageStore = challengeMessageStore;
        this.securityTokenStore = securityTokenStore;
        this.objectMapper = new ObjectMapper();
        controller.registerHandler(POST, "/auth", this);
        securityController.allow(POST, "/auth");

        this.pluginSettings = pluginSettings;
    }

    @Override
    protected void handleRequest(final RestRequest request, RestChannel channel, Client client) throws Exception {

        AuthToken token;
        try {
            token = objectMapper.readValue(request.content().toUtf8(), AuthToken.class);
        } catch(JsonProcessingException e) {
            channel.sendResponse(new BytesRestResponse(FORBIDDEN, Boolean.FALSE.toString()));
            return;
        }

        if (StringUtils.isBlank(token.pubkey)) {
            channel.sendResponse(new BytesRestResponse(FORBIDDEN, Boolean.FALSE.toString()));
            return;
        }

        // Only moderators or admin can authenticate
        if (!pluginSettings.getDocumentAdminAndModeratorsPubkeys().contains(token.pubkey)) {
            channel.sendResponse(new BytesRestResponse(FORBIDDEN));
            return;
        }

        // Check token validity
        if (!challengeMessageStore.validateToken(token)) {
            channel.sendResponse(new BytesRestResponse(FORBIDDEN, Boolean.FALSE.toString()));
            return;
        }

        String tokenStr = securityTokenStore.addToken(token);
        channel.sendResponse(new BytesRestResponse(OK, tokenStr));
    }


}