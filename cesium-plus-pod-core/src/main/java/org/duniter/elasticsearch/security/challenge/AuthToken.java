package org.duniter.elasticsearch.security.challenge;


import org.duniter.core.util.Preconditions;

public class AuthToken {
    public String pubkey;
    public String challenge;
    public String signature;


    public String toString() {
        return pubkey + ":" + challenge + "|" + signature;
    }

    public static AuthToken parse(String token) {
        Preconditions.checkNotNull(token);
        String[] parts = token.split(":");
        Preconditions.checkArgument(parts.length == 2);
        String[] subParts = parts[1].split("|");
        Preconditions.checkArgument(subParts.length == 2);

        AuthToken target = new AuthToken();
        target.pubkey = parts[0];
        target.challenge = subParts[0];
        target.signature = subParts[1];
        return target;
    }
}