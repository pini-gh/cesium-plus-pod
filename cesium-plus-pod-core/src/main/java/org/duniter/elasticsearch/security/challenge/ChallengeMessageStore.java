package org.duniter.elasticsearch.security.challenge;

/*
 * #%L
 * duniter4j :: UI Wicket
 * %%
 * Copyright (C) 2014 - 2016 EIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.duniter.core.service.CryptoService;
import org.duniter.core.util.Preconditions;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.Cache;
import org.duniter.core.util.ObjectUtils;
import org.duniter.core.util.StringUtils;
import org.duniter.core.util.crypto.CryptoUtils;
import org.duniter.elasticsearch.PluginSettings;
import org.elasticsearch.common.inject.Inject;
import org.elasticsearch.common.logging.ESLogger;
import org.elasticsearch.common.logging.ESLoggerFactory;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.rest.BytesRestResponse;

import java.util.concurrent.TimeUnit;

import static org.elasticsearch.rest.RestStatus.OK;

/**
 * Created by blavenie on 06/01/16.
 */
public class ChallengeMessageStore {

    private static final ESLogger log = ESLoggerFactory.getLogger(ChallengeMessageStore.class.getName());


    private String prefix;
    private long validityDurationInSeconds;
    private Cache<String, String> store;

    private final PluginSettings pluginSettings;

    private final CryptoService cryptoService;

    @Inject
    public ChallengeMessageStore(Settings settings,
                                 PluginSettings pluginSettings,
                                 CryptoService cryptoService) {
        this.prefix = pluginSettings.getSoftwareName() + "-";
        this.validityDurationInSeconds = settings.getAsInt("duniter4j.auth.challengeValidityDuration", 10);
        this.pluginSettings = pluginSettings;
        this.cryptoService = cryptoService;
        this.store = initGeneratedMessageCache();
    }

    public boolean validateToken(AuthToken token) {
        Preconditions.checkNotNull(token);
        Preconditions.checkArgument(StringUtils.isNotBlank(token.challenge));

        String storedChallenge = store.getIfPresent(token.challenge);

        // if no value in cache => maybe challenge expired
        if (!ObjectUtils.equals(storedChallenge, token.challenge)) {
            return false;
        }

        if (!cryptoService.verify(token.challenge, token.signature, token.pubkey)) {
            return false; // Invalid signature
        }
        store.invalidate(token.challenge);
        return true;
    }

    public AuthToken createNewChallenge() {
        String challenge = newChallenge();

        AuthToken result = new AuthToken();
        result.challenge = challenge;
        result.pubkey = pluginSettings.getNodePubkey();
        result.signature = cryptoService.sign(challenge, pluginSettings.getNodeKeypair().getSecKey());

        if (log.isDebugEnabled()) log.debug("New authentication challenge: " + challenge);
        store.put(challenge, challenge);

        return result;
    }

    /* -- internal methods -- */

    protected String newChallenge() {
        byte[] randomNonce = cryptoService.getBoxRandomNonce();
        String randomNonceStr = CryptoUtils.encodeBase64(randomNonce);
        return cryptoService.hash(randomNonceStr);
    }

    protected Cache<String, String> initGeneratedMessageCache() {
        return CacheBuilder.newBuilder()
                .expireAfterWrite(validityDurationInSeconds, TimeUnit.SECONDS)
                .build();
    }
}
