package org.duniter.elasticsearch.util;

import org.duniter.core.util.Beans;
import org.duniter.core.util.StringUtils;
import org.duniter.core.util.http.InetAddressUtils;
import org.elasticsearch.rest.RestRequest;

import java.net.InetSocketAddress;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RestUtils extends org.elasticsearch.rest.support.RestUtils {

    public static final String X_REAL_IP_HEADER = "X-Real-IP";
    public static final String X_FORWARD_FOR_HEADER = "X-Forwarded-For";
    protected RestUtils() {
        // Helper class
    }

    public static Map<String, String> getHeaders(RestRequest request) {
        Map<String, String> headers = Beans.getStream(request.headers())
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (k1, k2) -> k1));
        return headers;
    }

    public static String getIPAddress(RestRequest request) {

        return Stream.of(request.header(X_REAL_IP_HEADER), request.header(X_FORWARD_FOR_HEADER))
            // Filter valid only
            .filter(RestUtils::isNotLocalAddress)
            .findFirst()
            // If not found, use the host address
            .orElseGet(() -> {
                InetSocketAddress remoteAddress = (InetSocketAddress)request.getRemoteAddress();
                String remoteHost = (remoteAddress != null)
                    ? remoteAddress.getAddress().getHostAddress()
                    : null;
                return isNotLocalAddress(remoteHost) ? remoteHost : null;
            });
    }

    protected static boolean isNotLocalAddress(String input) {
        return StringUtils.isNotBlank(input) && InetAddressUtils.isNotLocalAddress(input);
    }
}
