package org.duniter.elasticsearch.dao.blockchain;

public interface IBlockRepository {
    /**
     * Name of ES index type (e.g. 'g1/block')
     */
    String TYPE = "block";
    String CURRENT_BLOCK_ID = "current";
}
