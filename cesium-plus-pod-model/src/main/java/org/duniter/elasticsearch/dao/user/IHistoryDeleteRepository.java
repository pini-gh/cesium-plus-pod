package org.duniter.elasticsearch.dao.user;

public interface IHistoryDeleteRepository {
    /**
     * Name of ES index
     */
    String INDEX = "history";
    String TYPE = "delete";
}
