package org.duniter.elasticsearch.dao.user;

public interface IUserProfileRepository {
    /**
     * Name of ES index
     */
    String INDEX = "user";
    String TYPE = "profile";
}
