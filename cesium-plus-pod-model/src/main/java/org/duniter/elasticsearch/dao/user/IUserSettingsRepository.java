package org.duniter.elasticsearch.dao.user;

public interface IUserSettingsRepository {
    /**
     * Name of ES index
     */
    String INDEX = "user";
    String TYPE = "settings";
}
