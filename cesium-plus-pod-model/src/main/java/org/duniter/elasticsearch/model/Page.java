package org.duniter.elasticsearch.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Page {

    public static final Page DEFAULT = Page.builder().build();

    public static Page nullToDefault(Page page) {
            return page != null ? page : DEFAULT;
    }
    @Builder.Default
    private Integer from = 0;

    @Builder.Default
    private Integer size = 1000;

    @Builder.Default
    private String sortBy = null;

    @Builder.Default
    private SortDirection sortDirection = SortDirection.ASC;

}
