package org.duniter.elasticsearch.model;

/*
 * #%L
 * Duniter4j :: Core Client API
 * %%
 * Copyright (C) 2014 - 2016 EIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Helper class
 * Created by blavenie on 01/03/16.
 */
public final class Records {

    public static final int PROTOCOL_VERSION = 2;

    @NoArgsConstructor(access = AccessLevel.PROTECTED)
    public static class Fields extends Record.Fields {
       
        public static final String CREATION_TIME="creationTime";
    
        // Read marker
        public static final String READ_SIGNATURE="readSignature";
    
        // Location
        public static final String ADDRESS="address";
        public static final String CITY="city";
        public static final String GEO_POINT="geoPoint";
    
        // record
        public static final String TITLE="title";
        public static final String DESCRIPTION="description";
    
        // Avatar & pictures
        public static final String AVATAR="avatar";
        public static final String PICTURES="pictures";
        public static final String PICTURES_COUNT="picturesCount";
    
        // Socials & tags
        public static final String SOCIALS="socials";
        public static final String TAGS="tags";

        // Other
        public static final String CATEGORY="category";
        public static final String CONTENT="content";

    }

    public class JsonFields {
        public static final String READ_SIGNATURE="read_signature";
    }

}
