package org.duniter.elasticsearch.model;

public enum SortDirection {
    ASC,
    DESC;

    public static SortDirection parse(String direction) {
        return direction != null ? SortDirection.valueOf(direction.toUpperCase()) : null;
    }

    public static SortDirection parse(String direction, SortDirection defaultValue) {
        return direction != null ? SortDirection.valueOf(direction.toUpperCase()) : defaultValue;
    }

}
