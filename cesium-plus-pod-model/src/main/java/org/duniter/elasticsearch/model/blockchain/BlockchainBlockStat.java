package org.duniter.elasticsearch.model.blockchain;

/*
 * #%L
 * Duniter4j :: Core Client API
 * %%
 * Copyright (C) 2014 - 2016 EIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.Data;
import lombok.experimental.FieldNameConstants;

import java.io.Serializable;
import java.math.BigInteger;

/**
 * Created by blavenie on 29/11/16.
 */
@Data
@FieldNameConstants
public class BlockchainBlockStat implements Serializable {


    // Property copied from Block
    private int version;
    private String currency;
    private Integer number;
    private String issuer;
    private String hash;
    private Long medianTime;
    private Integer membersCount;
    private BigInteger monetaryMass;
    private Integer unitbase;
    private BigInteger dividend;

    // Statistics
    private Integer txCount;
    private BigInteger txAmount;
    private Integer txChangeCount;
    private Integer certCount;


}
