package org.duniter.elasticsearch.model.query;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.experimental.FieldNameConstants;

@Data
@FieldNameConstants
@JsonIgnoreProperties(ignoreUnknown=true)
public class SearchHits {

    private SearchHit[] hits;
    private Long total;

}