package org.duniter.elasticsearch.model.stats;

/*
 * #%L
 * Duniter4j :: ElasticSearch Core plugin
 * %%
 * Copyright (C) 2014 - 2017 EIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by blavenie on 30/12/16.
 */
@Data
@FieldNameConstants
public class SaveResult implements Serializable {

    @NoArgsConstructor(access = AccessLevel.PROTECTED)
    public static class Fields {}

    private long inserts = 0;
    private long updates = 0;
    private long deletes = 0;

    @FieldNameConstants.Exclude
    private Map<String, Long> insertHits = new HashMap<>();
    @FieldNameConstants.Exclude
    private Map<String, Long> updateHits = new HashMap<>();
    @FieldNameConstants.Exclude
    private Map<String, Long> deleteHits = new HashMap<>();

    public void addInserts(String index, String type, long nbHits) {
        insertHits.put(index + "/" + type, getInserts(index, type) + nbHits);
        inserts += nbHits;
    }

    public void addUpdates(String index, String type, long nbHits) {
        updateHits.put(index + "/" + type, getUpdates(index, type) + nbHits);
        updates += nbHits;
    }


    public void addDeletes(String index, String type, long nbHits) {
        deleteHits.put(index + "/" + type, getDeletes(index, type) + nbHits);
        deletes += nbHits;
    }

    @JsonIgnore
    public long getInserts(String index, String type) {
        return insertHits.getOrDefault(index + "/" + type, 0l);
    }

    @JsonIgnore
    public long getUpdates(String index, String type) {
        return updateHits.getOrDefault(index + "/" + type, 0l);
    }

    @JsonIgnore
    public long getDeletes(String index, String type) {
        return deleteHits.getOrDefault(index + "/" + type, 0l);
    }

    @JsonIgnore
    public long getTotal() {
        return inserts + updates + deletes;
    }

    public String toString() {
        return String.format("%s insertions, %s updates, %s deletions",
            inserts,
            updates,
            deletes);
    }
}
