package org.duniter.elasticsearch.model.type;

/*
 * #%L
 * Duniter4j :: Core Client API
 * %%
 * Copyright (C) 2014 - 2016 EIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import lombok.*;
import lombok.experimental.FieldNameConstants;

/**
 * Created by blavenie on 01/03/16.
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldNameConstants
public class GeoPoint {

    @NoArgsConstructor(access = AccessLevel.PROTECTED)
    public static class Fields {
    }

    private Double lat;

    private Double lon;

}
