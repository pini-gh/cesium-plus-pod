package org.duniter.elasticsearch.subscription.dao.execution;

/*-
 * #%L
 * Duniter4j :: ElasticSearch Subscription plugin
 * %%
 * Copyright (C) 2014 - 2017 EIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.duniter.core.exception.TechnicalException;
import org.duniter.core.util.Preconditions;
import org.duniter.elasticsearch.subscription.PluginSettings;
import org.duniter.elasticsearch.subscription.dao.AbstractSubscriptionIndexTypeRepository;
import org.duniter.elasticsearch.subscription.dao.SubscriptionIndexRepository;
import org.duniter.elasticsearch.model.subscription.SubscriptionExecution;
import org.duniter.elasticsearch.model.subscription.SubscriptionRecord;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.common.inject.Inject;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.sort.SortOrder;

import java.io.IOException;

/**
 * Created by blavenie on 03/04/17.
 */
public class SubscriptionExecutionRepositoryImpl extends AbstractSubscriptionIndexTypeRepository<SubscriptionExecutionRepositoryImpl> implements SubscriptionExecutionRepository<SubscriptionExecutionRepositoryImpl> {

    @Inject
    public SubscriptionExecutionRepositoryImpl(PluginSettings pluginSettings, SubscriptionIndexRepository indexDao) {
        super(SubscriptionIndexRepository.INDEX, TYPE, pluginSettings);

        indexDao.register(this);
    }

    @Override
    public SubscriptionExecution getLastExecution(SubscriptionRecord record) {
        Preconditions.checkNotNull(record);
        Preconditions.checkNotNull(record.getIssuer());
        Preconditions.checkNotNull(record.getType());
        Preconditions.checkNotNull(record.getId());

        return getLastExecution(record.getIssuer(), record.getType(), record.getId());
    }

    @Override
    public SubscriptionExecution getLastExecution(String recipient, String recordType, String recordId) {

        BoolQueryBuilder query = QueryBuilders.boolQuery()
                .filter(QueryBuilders.termQuery(SubscriptionExecution.Fields.RECIPIENT, recipient))
                .filter(QueryBuilders.termsQuery(SubscriptionExecution.Fields.RECORD_TYPE, recordType))
                .filter(QueryBuilders.termQuery(SubscriptionExecution.Fields.RECORD_ID, recordId));

        SearchResponse response = client.prepareSearch(SubscriptionIndexRepository.INDEX)
                .setTypes(SubscriptionExecutionRepository.TYPE)
                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                .setQuery(QueryBuilders.constantScoreQuery(query))
                .setFetchSource(true)
                .setFrom(0).setSize(1)
                .addSort(SubscriptionExecution.Fields.TIME, SortOrder.DESC)
                .get();

        if (response.getHits().getTotalHits() == 0) return null;

        SearchHit hit = response.getHits().getHits()[0];
        return client.readSourceOrNull(hit, SubscriptionExecution.class);
    }

    @Override
    public Long getLastExecutionTime(SubscriptionRecord record) {
        Preconditions.checkNotNull(record);
        Preconditions.checkNotNull(record.getIssuer());
        Preconditions.checkNotNull(record.getType());
        Preconditions.checkNotNull(record.getId());

        return getLastExecutionTime(record.getIssuer(), record.getType(), record.getId());
    }

    @Override
    public Long getLastExecutionTime(String recipient, String recordType, String recordId) {

        BoolQueryBuilder query = QueryBuilders.boolQuery()
                .must(QueryBuilders.termQuery(SubscriptionExecution.Fields.RECIPIENT, recipient))
                .must(QueryBuilders.termQuery(SubscriptionExecution.Fields.RECORD_ID, recordId))
                .must(QueryBuilders.termsQuery(SubscriptionExecution.Fields.RECORD_ID, recordType));

        SearchResponse response = client.prepareSearch(SubscriptionIndexRepository.INDEX)
                .setTypes(SubscriptionExecutionRepository.TYPE)
                .setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
                .setQuery(query)
                .addField(SubscriptionExecution.Fields.TIME)
                .setFrom(0).setSize(1)
                .addSort(SubscriptionExecution.Fields.TIME, SortOrder.DESC)
                .get();

        if (response.getHits().getTotalHits() == 0) return null;
        SearchHit hit = response.getHits().getHits()[0];
        return hit.field(SubscriptionExecution.Fields.TIME).getValue();
    }

    @Override
    public XContentBuilder createTypeMapping() {
        try {
            XContentBuilder mapping = XContentFactory.jsonBuilder().startObject()
                    .startObject(getType())
                    .startObject("properties")

                    // issuer
                    .startObject(SubscriptionExecution.Fields.ISSUER)
                    .field("type", "string")
                    .field("index", "not_analyzed")
                    .endObject()

                    // recipient
                    .startObject(SubscriptionExecution.Fields.RECIPIENT)
                    .field("type", "string")
                    .field("index", "not_analyzed")
                    .endObject()

                    // record type
                    .startObject(SubscriptionExecution.Fields.RECORD_TYPE)
                    .field("type", "string")
                    .field("index", "not_analyzed")
                    .endObject()

                    // record id
                    .startObject(SubscriptionExecution.Fields.RECORD_ID)
                    .field("type", "string")
                    .field("index", "not_analyzed")
                    .endObject()

                    // time
                    .startObject(SubscriptionExecution.Fields.TIME)
                    .field("type", "integer")
                    .endObject()

                    // hash
                    .startObject(SubscriptionExecution.Fields.HASH)
                    .field("type", "string")
                    .field("index", "not_analyzed")
                    .endObject()

                    // signature
                    .startObject(SubscriptionExecution.Fields.SIGNATURE)
                    .field("type", "string")
                    .field("index", "not_analyzed")
                    .endObject()

                    .endObject()
                    .endObject().endObject();

            return mapping;
        }
        catch(IOException ioe) {
            throw new TechnicalException(String.format("Error while getting mapping for index [%s/%s]: %s", getIndex(), getType(), ioe.getMessage()), ioe);
        }
    }

}
