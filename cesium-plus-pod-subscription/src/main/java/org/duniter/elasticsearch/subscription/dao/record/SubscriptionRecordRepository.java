package org.duniter.elasticsearch.subscription.dao.record;

/*-
 * #%L
 * Duniter4j :: ElasticSearch Subscription plugin
 * %%
 * Copyright (C) 2014 - 2017 EIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import org.duniter.elasticsearch.model.Page;
import org.duniter.elasticsearch.model.subscription.SubscriptionRecord;
import org.duniter.elasticsearch.subscription.dao.SubscriptionIndexTypeRepository;
import org.elasticsearch.index.query.QueryBuilder;

import javax.annotation.Nullable;
import java.util.List;

/**
 * Created by blavenie on 03/04/17.
 */
public interface SubscriptionRecordRepository<T extends SubscriptionIndexTypeRepository> extends SubscriptionIndexTypeRepository<T> {

    String TYPE = "record";

    <T extends SubscriptionRecord<?>> List<T> findAllByRecipient(String recipient, Class<? extends T> clazz, @Nullable Page page, String... types);

    <T extends SubscriptionRecord<?>> List<T> findAll(QueryBuilder query, Class<? extends T> clazz,  @Nullable Page page);
}
