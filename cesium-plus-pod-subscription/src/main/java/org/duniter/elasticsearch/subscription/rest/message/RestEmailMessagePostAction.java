package org.duniter.elasticsearch.subscription.rest.message;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.net.HttpHeaders;
import org.duniter.core.client.model.bma.Constants;
import org.duniter.core.client.model.bma.jackson.JacksonUtils;
import org.duniter.core.exception.TechnicalException;
import org.duniter.core.util.ArrayUtils;
import org.duniter.core.util.StringUtils;
import org.duniter.elasticsearch.model.user.Message;
import org.duniter.elasticsearch.rest.security.RestSecurityController;
import org.duniter.elasticsearch.security.challenge.AuthToken;
import org.duniter.elasticsearch.security.token.SecurityTokenStore;
import org.duniter.elasticsearch.subscription.PluginSettings;
import org.duniter.elasticsearch.subscription.service.SubscriptionService;
import org.duniter.elasticsearch.user.service.MessageService;
import org.duniter.elasticsearch.util.RestUtils;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.inject.Inject;
import org.elasticsearch.common.logging.ESLogger;
import org.elasticsearch.common.logging.Loggers;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.rest.*;

import java.io.IOException;
import java.util.Arrays;
import java.util.regex.Pattern;

import static org.elasticsearch.rest.RestRequest.Method.POST;
import static org.elasticsearch.rest.RestStatus.FORBIDDEN;
import static org.elasticsearch.rest.RestStatus.UNAUTHORIZED;

/**
 *
 */
public class RestEmailMessagePostAction extends BaseRestHandler {

    public final static String TOKEN = "token";
    public final static String PATH = String.format("/%s/_email", MessageService.INDEX, MessageService.INBOX_TYPE);

    public final static Pattern PUBKEY_PATTERN = Pattern.compile("^" + Constants.Regex.PUBKEY + "$");

    private final ESLogger log;

    private final SecurityTokenStore securityTokenStore;

    private final SubscriptionService subscriptionService;

    private final PluginSettings pluginSettings;

    @Inject
    public RestEmailMessagePostAction(Settings settings, RestController controller,
                                      Client client,
                                      PluginSettings pluginSettings,
                                      RestSecurityController securityController,
                                      SecurityTokenStore securityTokenStore,
                                      SubscriptionService subscriptionService
                                      ) {
        super(settings, controller, client);
        log = Loggers.getLogger("duniter.rest" + MessageService.INDEX, settings, String.format("[%s]", MessageService.INDEX));
        this.securityTokenStore = securityTokenStore;
        this.subscriptionService = subscriptionService;
        this.pluginSettings = pluginSettings;
        securityController.allow(POST, PATH);
        controller.registerHandler(POST, PATH, this);
    }

    @Override
    public void handleRequest(final RestRequest request, final RestChannel channel, final Client client) throws Exception {
        // Check validation token
        String authorization = request.header(HttpHeaders.AUTHORIZATION);
        if (authorization == null || !authorization.startsWith(TOKEN)) {
            channel.sendResponse(new BytesRestResponse(UNAUTHORIZED));
            return;
        }

        String token = authorization.substring(TOKEN.length()).trim();

        if (!securityTokenStore.validateToken(token)) {
            String ip = RestUtils.getIPAddress(request);
            log.warn("Reject request to [{}] from {{}} - Invalid token: {}", PATH, ip, token);
            channel.sendResponse(new BytesRestResponse(FORBIDDEN));
            return;
        }

        AuthToken authToken = AuthToken.parse(token);
        if (!pluginSettings.getDelegate().getDocumentAdminAndModeratorsPubkeys().contains(authToken.pubkey)) {
            String ip = RestUtils.getIPAddress(request);
            log.warn("Reject request to [{}] from {{}}", PATH, ip, token);
            channel.sendResponse(new BytesRestResponse(FORBIDDEN));
            return;
        }

        ObjectMapper om = JacksonUtils.getThreadObjectMapper();
        try {
            Message message = om.readValue(request.content().toUtf8(), Message.class);

            String[] recipients = null;
            if (StringUtils.isNotBlank(message.getRecipient())) {
                recipients = Arrays.stream(message.getRecipient().split(","))
                    .map(String::trim)
                    .filter(recipient -> PUBKEY_PATTERN.matcher(recipient).matches())
                    .toArray(String[]::new);
            }

            // Only admin can send to many recipients
            if (ArrayUtils.size(recipients) != 1 && !pluginSettings.getNodePubkey().equals(authToken.pubkey)) {
                String ip = RestUtils.getIPAddress(request);
                log.warn("Reject request to [{}] from {{}} - Only admin can send to many recipients", PATH, ip, token);
                channel.sendResponse(new BytesRestResponse(FORBIDDEN));
                return;
            }

            subscriptionService.sendEmailMessage(message.getTitle(),
                message.getContent(),
                recipients);

        } catch (IOException e) {
            throw new TechnicalException(e);
        }

    }

}