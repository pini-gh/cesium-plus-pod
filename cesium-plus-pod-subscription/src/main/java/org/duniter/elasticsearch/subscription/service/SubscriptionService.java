package org.duniter.elasticsearch.subscription.service;

/*
 * #%L
 * Duniter4j :: Core API
 * %%
 * Copyright (C) 2014 - 2015 EIS
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.duniter.core.client.model.ModelUtils;
import org.duniter.core.exception.TechnicalException;
import org.duniter.core.service.CryptoService;
import org.duniter.core.util.*;
import org.duniter.core.util.crypto.CryptoUtils;
import org.duniter.elasticsearch.client.Duniter4jClient;
import org.duniter.elasticsearch.model.Page;
import org.duniter.elasticsearch.model.Record;
import org.duniter.elasticsearch.model.subscription.SubscriptionExecution;
import org.duniter.elasticsearch.model.subscription.SubscriptionRecord;
import org.duniter.elasticsearch.model.subscription.email.EmailSubscription;
import org.duniter.elasticsearch.model.user.DocumentReference;
import org.duniter.elasticsearch.model.user.Message;
import org.duniter.elasticsearch.model.user.UserEvent;
import org.duniter.elasticsearch.subscription.PluginSettings;
import org.duniter.elasticsearch.subscription.dao.execution.SubscriptionExecutionRepository;
import org.duniter.elasticsearch.subscription.dao.record.SubscriptionRecordRepository;
import org.duniter.elasticsearch.threadpool.ScheduledActionFuture;
import org.duniter.elasticsearch.threadpool.ThreadPool;
import org.duniter.elasticsearch.user.service.AdminService;
import org.duniter.elasticsearch.user.service.MailService;
import org.duniter.elasticsearch.user.service.UserEventService;
import org.duniter.elasticsearch.user.service.UserService;
import org.duniter.elasticsearch.util.springtemplate.STUtils;
import org.elasticsearch.common.inject.Inject;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.nuiton.i18n.I18n;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroup;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Created by Benoit on 30/03/2015.
 */
public class SubscriptionService extends AbstractService {

    private static final Locale DEFAULT_LOCALE = new Locale("en", "GB");

    private SubscriptionRecordRepository<?> subscriptionRecordRepository;
    private SubscriptionExecutionRepository subscriptionExecutionDao;
    private ThreadPool threadPool;
    private MailService mailService;
    private AdminService adminService;
    private UserEventService userEventService;
    private UserService userService;
    private String emailSubjectPrefix;
    private String emailLinkName;
    private STGroup templates;
    private boolean debug;

    @Inject
    public SubscriptionService(Duniter4jClient client,
                               PluginSettings settings,
                               CryptoService cryptoService,
                               SubscriptionRecordRepository subscriptionRecordRepository,
                               SubscriptionExecutionRepository subscriptionExecutionDao,
                               ThreadPool threadPool,
                               MailService mailService,
                               AdminService adminService,
                               UserService userService,
                               UserEventService userEventService) {
        super("duniter.subscription", client, settings, cryptoService);
        this.subscriptionRecordRepository = subscriptionRecordRepository;
        this.subscriptionExecutionDao = subscriptionExecutionDao;
        this.threadPool = threadPool;
        this.mailService = mailService;
        this.adminService = adminService;
        this.userService = userService;
        this.userEventService = userEventService;
        this.emailSubjectPrefix = pluginSettings.getMailSubjectPrefix().trim();
        if (StringUtils.isNotBlank(emailSubjectPrefix)) {
            emailSubjectPrefix += " "; // add one trailing space
        }
        this.emailLinkName = pluginSettings.getEmailLinkName().trim();
        this.debug = logger.isDebugEnabled();

        // Configure springtemplate engine
        STGroup.verbose = debug;
        templates = STUtils.newSTGroup("org/duniter/elasticsearch/subscription/templates");
        Preconditions.checkNotNull(templates.getInstanceOf("text_email"), "Missing ST template {text_email}");
        Preconditions.checkNotNull(templates.getInstanceOf("html_email_content"), "Missing ST template {html_email_content}");
    }

    public String create(String json) {
        JsonNode actualObj = readAndVerifyIssuerSignature(json);
        String issuer = getIssuer(actualObj);

        if (logger.isDebugEnabled()) {
            logger.debug(String.format("Indexing a subscription from issuer [%.8s]", issuer));
        }

        return subscriptionRecordRepository.create(json);
    }

    public void update(String id, String json) {
        JsonNode actualObj = readAndVerifyIssuerSignature(json);
        String issuer = getIssuer(actualObj);

        // Check same document issuer
        subscriptionRecordRepository.checkSameDocumentIssuer(id, issuer);

        if (logger.isDebugEnabled()) {
            logger.debug(String.format("Updating subscription {%s} from issuer {%.8s}", id, issuer));
        }

        subscriptionRecordRepository.update(id, json);
    }

    public SubscriptionService startScheduling() {
        if (!pluginSettings.getMailEnable()) {
            logger.warn(I18n.t("duniter4j.es.subscription.error.mailDisabling"));
            return this;
        }

        // Email subscriptions
        {
            if (logger.isInfoEnabled()) {
                Calendar cal = new GregorianCalendar();
                cal.setTimeInMillis(0);
                cal.set(Calendar.DAY_OF_WEEK, pluginSettings.getEmailSubscriptionsExecuteDayOfWeek());
                String dayOfWeek = new SimpleDateFormat("EEE").format(cal.getTime());
                logger.warn(I18n.t("duniter4j.es.subscription.email.start", pluginSettings.getEmailSubscriptionsExecuteHour(), dayOfWeek));
            }

            // if DEBUG mode enable
            if (pluginSettings.isEmailSubscriptionsDebug()) {
                threadPool.schedule(
                        () -> sendEmailMessage("Subscription email test", "Subscription test message<br>"
                            +" (debug mode has been enable in config). <u>test underline</u>"),
                        new TimeValue(10, TimeUnit.SECONDS) /* after 10s */
                );
            }
            // Execution at startup (or DEBUG mode)
            else if (pluginSettings.isEmailSubscriptionsExecuteAtStartup()) {
                threadPool.schedule(
                    () -> executeEmailSubscriptions(EmailSubscription.Frequency.daily),
                    new TimeValue(20, TimeUnit.SECONDS) /* after 20s */
                );
            }

            // Daily execution
            threadPool.scheduleAtFixedRate(
                    () -> executeEmailSubscriptions(EmailSubscription.Frequency.daily),
                    DateUtils.delayBeforeHour(pluginSettings.getEmailSubscriptionsExecuteHour()),
                    DateUtils.DAY_DURATION_IN_MILLIS,
                    TimeUnit.MILLISECONDS);

            // Weekly execution
            threadPool.scheduleAtFixedRate(
                    () -> executeEmailSubscriptions(EmailSubscription.Frequency.weekly),
                    DateUtils.delayBeforeDayAndHour(pluginSettings.getEmailSubscriptionsExecuteDayOfWeek(), pluginSettings.getEmailSubscriptionsExecuteHour()),
                    7 * DateUtils.DAY_DURATION_IN_MILLIS,
                    TimeUnit.MILLISECONDS);
        }
        return this;
    }

    public void executeEmailSubscriptions(final EmailSubscription.Frequency frequency) {

        long now = System.currentTimeMillis();
        logger.info(String.format("Executing %s email subscription...", frequency.name()));
        boolean debug = pluginSettings.isEmailSubscriptionsDebug();

        final String senderPubkey = pluginSettings.getNodePubkey();

        Page page = Page.builder().from(0).size(100).build();
        boolean hasMore = true;
        long executionCount=0;
        while (hasMore) {
            List<EmailSubscription> subscriptions = subscriptionRecordRepository.findAllByRecipient(senderPubkey,
                EmailSubscription.class,
                page,
                EmailSubscription.TYPE);

            // Get profiles titles, for issuers and the sender
            Set<String> issuers =  subscriptions.stream()
                    .map(SubscriptionRecord::getIssuer)
                    .collect(Collectors.toSet());
            final Map<String, String> profileTitles = userService.getProfileTitles(
                    ImmutableSet.<String>builder().addAll(issuers).add(senderPubkey).build());
            final String senderName = (profileTitles != null && profileTitles.containsKey(senderPubkey)) ? profileTitles.get(senderPubkey) :
                ModelUtils.minifyPubkey(senderPubkey);

            executionCount += subscriptions.stream()
                    .map(this::decryptEmailSubscription)
                    .filter(record -> (record != null && record.getContent().getFrequency() == frequency))
                    .map(record -> processEmailSubscription(record, senderPubkey, senderName, profileTitles, debug))
                    .filter(Objects::nonNull)
                    .map(this::saveExecution)
                    .count();

            hasMore = CollectionUtils.size(subscriptions) >= page.getSize();
            page.setFrom(page.getFrom() + page.getSize());
        }

        logger.info(String.format("Executing %s email subscription... [OK] emails sent [%s] (in %s ms)",
                frequency.name(), executionCount, System.currentTimeMillis()-now));
    }

    public void sendEmailMessage(String title, String body, String... pubkeys) {
        Preconditions.checkNotNull(title);
        Preconditions.checkNotNull(body);

        long now = System.currentTimeMillis();
        logger.info("Sending %s email message to {} receivers...", pubkeys.length);
        boolean debug = pluginSettings.isEmailSubscriptionsDebug();

        final String senderPubkey = pluginSettings.getNodePubkey();

        BoolQueryBuilder query = QueryBuilders.boolQuery()
            .filter(QueryBuilders.termQuery(SubscriptionRecord.Fields.RECIPIENT, senderPubkey))
            .filter(QueryBuilders.termQuery(SubscriptionRecord.Fields.TYPE, EmailSubscription.TYPE));
        if (ArrayUtils.isNotEmpty(pubkeys)) {
            query.filter(QueryBuilders.termsQuery(SubscriptionRecord.Fields.ISSUER, pubkeys));
        }

        Page page = Page.builder()
            .from(0).size(100)
            .build();
        boolean hasMore = true;
        long executionCount = 0;
        while (hasMore) {
            List<EmailSubscription> subscriptions = subscriptionRecordRepository.findAll(QueryBuilders.constantScoreQuery(query),
                EmailSubscription.class, page);

            // Get profiles titles, for receivers and the sender
            List<String> receivers =  Beans.collectProperties(subscriptions, SubscriptionRecord.Fields.ISSUER);
            final Map<String, String> receiverNames = userService.getProfileTitles(
                ImmutableSet.<String>builder().addAll(receivers).add(senderPubkey).build());
            final String senderName = (receiverNames != null && receiverNames.containsKey(senderPubkey)) ? receiverNames.get(senderPubkey) :
                ModelUtils.minifyPubkey(senderPubkey);

            List<ScheduledActionFuture<?>> futures = subscriptions.stream()
                .map(this::decryptEmailSubscription)
                .filter(Objects::nonNull)
                .map(record -> sendEmailMessage(title, body, senderPubkey, senderName, record, receiverNames, debug))
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

            // Wait all email sent
            try {
                executionCount += ScheduledActionFuture.allOf(futures).get().size();
            } catch (InterruptedException | ExecutionException e) {
                // Error during email sent
                logger.error("Error while sending email");
            }

            hasMore = CollectionUtils.size(subscriptions) >= page.getSize();
            page.setFrom(page.getFrom() + page.getSize());
        }

        logger.info("Sending %s email message [OK] emails sent [{}] (in {} ms)",
            executionCount, System.currentTimeMillis()-now);

    }

    protected void sendEmailMessageByJson(String json) {
        ObjectMapper om = getObjectMapper();
        try {
            Message message = om.readValue(json, Message.class);

        } catch (IOException e) {
            throw new TechnicalException(e);
        }

    }

    /* -- protected methods -- */

    protected EmailSubscription decryptEmailSubscription(EmailSubscription subscription) {
        Preconditions.checkNotNull(subscription);
        Preconditions.checkNotNull(subscription.getId());

        if (StringUtils.isBlank(subscription.getRecipientContent()) || StringUtils.isBlank(subscription.getNonce()) ||
                StringUtils.isBlank(subscription.getIssuer())) {
            logger.error("Invalid subscription [{}]. Missing field 'recipientContent', 'nonce' or 'issuer'.", subscription.getId());
            return null;
        }

        String jsonContent;
        try {
            jsonContent = cryptoService.openBox(subscription.getRecipientContent(),
                    CryptoUtils.decodeBase58(subscription.getNonce()),
                    CryptoUtils.decodeBase58(subscription.getIssuer()),
                    pluginSettings.getNodeKeypair().getSecKey()
            );
        } catch(Exception e) {
            logger.error("Could not decrypt email subscription content for subscription [{}]", subscription.getId());
            return null;
        }

        try {
            EmailSubscription.Content content = getObjectMapper().readValue(jsonContent, EmailSubscription.Content.class);
            subscription.setContent(content);
        } catch(Exception e) {
            logger.error("Could not parse email subscription content [{}]: {}", jsonContent, e.getMessage());
            return null;
        }

        return subscription;
    }

    protected SubscriptionExecution processEmailSubscription(final EmailSubscription subscription,
                                                         final String senderPubkey,
                                                         final String senderName,
                                                         final Map<String, String> profileTitles,
                                                         boolean debug) {
        Preconditions.checkNotNull(subscription);

        if (subscription.getContent() != null && subscription.getContent().getEmail() != null) {
            if (debug) {
                logger.info("Processing email subscription from {{}} to {{}} (pubkey: {})",
                        senderName,
                        subscription.getContent().getEmail(),
                        ModelUtils.minifyPubkey(subscription.getIssuer()));
            }
            else {
                logger.info("Processing email subscription to {{}} (pubkey: {})",
                        subscription.getId(),
                        ModelUtils.minifyPubkey(subscription.getIssuer()));
            }
        }
        else {
            logger.warn("Processing email subscription {id={}}: no email found in content. Skipping", subscription.getId());
            return null;
        }

        SubscriptionExecution lastExecution = subscriptionExecutionDao.getLastExecution(subscription);
        Long lastExecutionTime;

        if (lastExecution != null) {
            lastExecutionTime = lastExecution.getTime();
        }
        // If first email execution: only sendBlock event from the last 7 days.
        else  {
            Calendar defaultDateLimit = new GregorianCalendar();
            defaultDateLimit.setTimeInMillis(System.currentTimeMillis());
            defaultDateLimit.add(Calendar.DAY_OF_YEAR, - 7);
            defaultDateLimit.set(Calendar.HOUR_OF_DAY, 0);
            defaultDateLimit.set(Calendar.MINUTE, 0);
            defaultDateLimit.set(Calendar.SECOND, 0);
            defaultDateLimit.set(Calendar.MILLISECOND, 0);
            lastExecutionTime = defaultDateLimit.getTimeInMillis() / 1000;
        }

        // Get last user events
        String[] includes = subscription.getContent() == null ? null : subscription.getContent().getIncludes();
        String[] excludes = subscription.getContent() == null ? null : subscription.getContent().getExcludes();
        List<UserEvent> userEvents = userEventService.getUserEvents(subscription.getIssuer(), lastExecutionTime, includes, excludes);

        if (CollectionUtils.isEmpty(userEvents)) return null; // no events: stop here

        // Get user locale
        Locale userLocale = getUserLocale(subscription, debug);

        final String title = emailSubjectPrefix + I18n.l(userLocale,"duniter4j.es.subscription.email.subject", userEvents.size());

        // Compute text content
        final String text = fillTemplate(
            templates.getInstanceOf("text_email"),
            title,
            subscription,
            senderPubkey,
            senderName,
            profileTitles,
            userEvents,
            true, // Show list divider
            userLocale,
            pluginSettings.getEmailHeaderColor(),
            pluginSettings.getEmailLogoUrl(),
            pluginSettings.getEmailLinkUrl(),
            emailLinkName)
            .render(userLocale);

        // Compute HTML content
        final String html = fillTemplate(
            templates.getInstanceOf("html_email_content"),
            title,
            subscription,
            senderPubkey,
            senderName,
            profileTitles,
            userEvents,
            true, // Show list divider
            userLocale,
            pluginSettings.getEmailHeaderColor(),
            pluginSettings.getEmailLogoUrl(),
            pluginSettings.getEmailLinkUrl(),
            emailLinkName)
            .render(userLocale);

        if (debug) {
            logger.info(String.format("---- Email to send (debug mode) ------\nTo:%s\nObject: %s\nText content:\n%s",
                    subscription.getContent().getEmail(),
                    title,
                    text));
            return null;
        }

        // Schedule email sending
        threadPool.schedule(() -> mailService.sendHtmlEmailWithText(
                title,
                text,
                html,
                subscription.getContent().getEmail()));

        // Compute last time (should be the first one, as events are sorted in DESC order)
        Long lastEventTime = userEvents.get(0).getTime();
        if (lastExecution == null) {
            lastExecution = new SubscriptionExecution();
            lastExecution.setRecipient(subscription.getIssuer());
            lastExecution.setRecordType(subscription.getType());
            lastExecution.setRecordId(subscription.getId());
        }
        lastExecution.setTime(lastEventTime);

        return lastExecution;
    }


    protected ScheduledActionFuture<?> sendEmailMessage(String title,
                                                     final String body,
                                                     final String senderPubkey,
                                                     final String senderName,
                                                     final EmailSubscription subscription,
                                                     final Map<String, String> profileTitles,
                                                     boolean debug) {
        Preconditions.checkNotNull(subscription);

        if (subscription.getContent() != null && subscription.getContent().getEmail() != null) {
            if (debug) {
                logger.info("Processing email message from {{}} to {{}} (pubkey: {})",
                    senderName,
                    subscription.getContent().getEmail(),
                    ModelUtils.minifyPubkey(subscription.getIssuer()));
            }
            else {
                logger.info("Processing email message to {{}} (pubkey: {})",
                    subscription.getId(),
                    ModelUtils.minifyPubkey(subscription.getIssuer()));
            }
        }
        else {
            logger.warn("Processing email subscription {id={}}: no email found in content. Skipping", subscription.getId());
            return null;
        }

        // Get user locale
        Locale userLocale = getUserLocale(subscription, debug);

        // Translate title
        final String subject = emailSubjectPrefix + I18n.l(userLocale, title);

        List<UserEvent> userEvents = ImmutableList.of(
            UserEvent.builder()
                .setMessage(body)
                .setType(UserEvent.EventType.INFO)
                .setTime(System.currentTimeMillis() / 1000)
                .build()
        );

        // Compute text content
        final String text = fillTemplate(
            templates.getInstanceOf("text_email"),
            subject,
            subscription,
            senderPubkey,
            senderName,
            profileTitles,
            userEvents,
            false, // Hide list divider
            userLocale,
            pluginSettings.getEmailHeaderColor(),
            pluginSettings.getEmailLogoUrl(),
            pluginSettings.getEmailLinkUrl(),
            emailLinkName)
            .render(userLocale);

        // Compute HTML content
        final String html = fillTemplate(
            templates.getInstanceOf("html_email_content"),
            subject,
            subscription,
            senderPubkey,
            senderName,
            profileTitles,
            userEvents,
            false, // Hide list divider
            userLocale,
            pluginSettings.getEmailHeaderColor(),
            pluginSettings.getEmailLogoUrl(),
            pluginSettings.getEmailLinkUrl(),
            emailLinkName)
            .render(userLocale);

        if (debug) {
            logger.info(String.format("---- Email to sendBlock (debug mode) ------\nTo:%s\nObject: %s\nText content:\n%s",
                subscription.getContent().getEmail(),
                subject,
                text));
        }

        // Schedule email sending
        return threadPool.schedule(() -> mailService.sendHtmlEmailWithText(
            subject,
            text,
            html,
            subscription.getContent().getEmail()));
    }

    public static ST fillTemplate(final ST template,
                                  String title,
                                  EmailSubscription subscription,
                                  String senderPubkey,
                                  String senderName,
                                  Map<String, String> issuerProfilNames,
                                  List<UserEvent> userEvents,
                                  boolean showListDivider,
                                  final Locale issuerLocale,
                                  String bgColor,
                                  String logoUrl,
                                  String linkUrl,
                                  String linkName) {
        String issuerName = issuerProfilNames != null && issuerProfilNames.containsKey(subscription.getIssuer()) ?
                issuerProfilNames.get(subscription.getIssuer()) :
                ModelUtils.minifyPubkey(subscription.getIssuer());

        // Remove comma (to avoid to be used as many args in the i18n_args template)
        issuerName = issuerName.replaceAll("[, ]+", " ");
        senderName = StringUtils.isNotBlank(senderName) ? senderName.replaceAll("[, ]+", " ") : senderName;

        try {
            // Compute body
            template.add("title", title);
            template.add("issuerPubkey", subscription.getIssuer());
            template.add("issuerName", issuerName);
            template.add("senderPubkey", senderPubkey);
            template.add("senderName", senderName);
            template.add("bgColor", bgColor);
            template.add("logoUrl", StringUtils.isBlank(logoUrl) ? null : logoUrl);
            template.add("url", StringUtils.isBlank(linkUrl) ? null : linkUrl);
            template.add("linkName", linkName);
            template.add("showOpenButton", StringUtils.isNotBlank(linkUrl) && StringUtils.isNotBlank(linkName));
            template.add("showListDivider", showListDivider);
            if (issuerLocale != null) template.add("locale", issuerLocale.getLanguage());
            userEvents.forEach(userEvent -> template.addAggr("events.{description, time}",
                        getUserEventDescription(issuerLocale, userEvent),
                        new Date(userEvent.getTime() * 1000)));

            return template;

        }
        catch (Exception e) {
          throw new TechnicalException(e);
        }
    }

    protected SubscriptionExecution saveExecution(SubscriptionExecution execution) {
        Preconditions.checkNotNull(execution);
        Preconditions.checkNotNull(execution.getRecipient());
        Preconditions.checkNotNull(execution.getRecordType());
        Preconditions.checkNotNull(execution.getRecordId());

        // Update issuer
        execution.setIssuer(pluginSettings.getNodePubkey());

        // Fill hash + signature
        String json = toJson(execution, true/*skip hash and signature*/);
        execution.setHash(cryptoService.hash(json));
        execution.setSignature(cryptoService.sign(json, pluginSettings.getNodeKeypair().getSecKey()));

        if (execution.getId() == null) {
            subscriptionExecutionDao.create(toJson(execution), false/*not wait*/);
        }
        else {
            subscriptionExecutionDao.update(execution.getId(), toJson(execution), false/*not wait*/);
        }
        return execution;
    }

    private String toJson(Record record) {
        return toJson(record, false);
    }

    private String toJson(Record record, boolean cleanHashAndSignature) {
        Preconditions.checkNotNull(record);
        try {
            String json = getObjectMapper().writeValueAsString(record);
            if (cleanHashAndSignature) {
                json = PARSER_SIGNATURE.removeFromJson(json);
                json = PARSER_HASH.removeFromJson(json);
            }
            return json;
        } catch(JsonProcessingException e) {
            throw new TechnicalException("Unable to serialize object " + record.getClass().getName(), e);
        }
    }

    private static String getUserEventDescription(Locale locale, UserEvent userEvent) {

        final String defaultKey = userEvent.getCode() != null
            ? "duniter.user.event." + userEvent.getCode().toUpperCase()
            : userEvent.getMessage();

        // Retrieve the translated description: prefer 'duniter.<INDEX>.event.<CODE>' if exists,
        // and 'duniter.user.event.<CODE>' otherwise
        final DocumentReference reference = userEvent.getReference();
        if (reference != null && reference.getIndex() != null) {
            return firstValidI18n(locale,
                    new String[]{
                        String.format("duniter.%s.%s.event.%s", userEvent.getReference().getIndex().toLowerCase(), userEvent.getReference().getType().toLowerCase(), userEvent.getCode().toUpperCase()),
                        String.format("duniter.%s.event.%s", userEvent.getReference().getIndex().toLowerCase(), userEvent.getCode().toUpperCase()),
                        defaultKey
                    },
                    userEvent.getParams());
        }

        return userEvent.getParams() != null ?
                I18n.l(locale, defaultKey, userEvent.getParams()) :
                I18n.l(locale, defaultKey);
    }

    /**
     * Return the first valid i18N translation
     * @param locale
     * @param keys
     * @param params
     * @return
     */
    private static String firstValidI18n(Locale locale, String[] keys, String[] params) {
        String result = null;
        for (String key: keys) {
            result = params != null ? I18n.l(locale, key, params) : I18n.l(locale, key);
            if (!key.equals(result)) {
                return result;
            }
        }
        return result;
    }

    private Locale getUserLocale(EmailSubscription subscription, boolean debug) {
        // Get user locale
        String[] localParts = subscription.getContent() != null
            && subscription.getContent().getLocale() != null
            ? subscription.getContent().getLocale().split("-") : null;
        if (localParts == null) return DEFAULT_LOCALE;

        Locale userLocale = localParts.length >= 2 ? new Locale(localParts[0].toLowerCase(), localParts[1].toUpperCase()) : new Locale(localParts[0].toLowerCase());

        if (debug) {
            String language = userLocale.getLanguage();
            if (!"fr".equals(language) && !"en".equals(language)) {
                logger.info("Detected an not supported language {}. Will use default {}", language, DEFAULT_LOCALE);
                return DEFAULT_LOCALE;
            }
        }

        return userLocale;
    }

}
