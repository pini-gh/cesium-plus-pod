# duniter/cesium-plus-pod

Cesium+ pod is a data pod and indexer for [Ğ1 libre-currency blockchain](https://duniter.org/). It is based on ElasticSearch.

# Example docker-compose file

```
version: "3.5"

services:
  cesium-plus-pod:
    image: duniter/cesium-plus-pod:latest
    ports:
      - 9200:9200
      - 9300:9300
    environment:
      # Network
      CESIUM_CURRENCY: "g1-test"
      CESIUM_CLUSTER_NAME: "my-cesium-pod"
      CESIUM_NODE_NAME: "node"
      CESIUM_REMOTE_HOST: "cesium.example.com"
      CESIUM_REMOTE_PORT: "443"
      CESIUM_DUNITER_HOST: "g1-test.duniter.org"
      CESIUM_DUNITER_PORT: "443"
      CESIUM_SYNC_HOSTS: "https://g1-test.data.duniter.org"
      # Email
      CESIUM_EMAIL_ENABLED: "false"
      # Key
      CESIUM_DUNITER_SALT: "my-duniter-salt"
      CESIUM_DUNITER_PASSWORD: "my-duniter-password"
    volumes:
      - cesium-config:/opt/cesium-plus-pod/config
      - cesium-data:/opt/cesium-plus-pod/data
      - cesium-logs:/opt/cesium-plus-pod/logs

volumes:
  cesium-config:
  cesium-data:
  cesium-logs:
```
# Environment variables

| Name | Description | Default |
| ---- | ----------- | ------- |
| `CESIUM_CLUSTER_NAME` | ElasticSearch cluster name | `cesium-plus-pod-$CESIUM_CURRENCY` |
| `CESIUM_CURRENCY` | The duniter based currency to use | `g1` |
| `CESIUM_CUSTOM_CONFIGURATION_FILE` | When set to `true` all other `CESIUM_*` environment variables are ignored and the configuration file is left untouched | `false` |
| `CESIUM_DUNITER_HOST` | Name of the duniter node to connect to | `$CESIUM_CURRENCY.duniter.org` |
| `CESIUM_DUNITER_PASSWORD` | Password of the duniter account to associate the pod with | Empty |
| `CESIUM_DUNITER_PORT` | Port of the duniter node to connect to | `443` |
| `CESIUM_DUNITER_SALT` | Salt of the duniter account to associate the pod with | Empty |
| `CESIUM_EMAIL_ENABLED` | Boolean (`true` / `false`) to enable the pod's email feature | `false` |
| `CESIUM_EMAIL_FROM` | From address to use when sending emails | `noreply@example.com` |
| `CESIUM_EMAIL_PREFIX` | Prefix to add to the subject of emails | `[Cesium+]` |
| `CESIUM_LISTEN_ADDR` | IP address to listen on | `0.0.0.0` |
| `CESIUM_MAX_RESULT_WINDOW` | Elasticsearch configuration item `index.max_result_window`. Its default value was bumped to 20000 to avoid `Result window is too large` errors | 20000 |
| `CESIUM_NODE_NAME` | ElasticSearch node name | The container's hostname |
| `CESIUM_REMOTE_HOST` | The public DNS name of the pod | `localhost` |
| `CESIUM_REMOTE_PORT` | The port to use to connect to the node from the outside | `9200` |
| `CESIUM_SMTP_HOST` | Name of the email server | `localhost` |
| `CESIUM_SMTP_PASSWORD` | Password to authenticate with on the email server | Empty |
| `CESIUM_SMTP_PORT` | Port of the email server | `25` |
| `CESIUM_SMTP_SSL` | Boolean to use SSL with the email server | `false` |
| `CESIUM_SMTP_STARTTLS` | Boolean to use STARTTLS with the email server | `false` |
| `CESIUM_SMTP_USERNAME` | Username to authenticate with on the email server | Empty |
| `CESIUM_SYNC_HOSTS` | Initial list of pods' URL to sync with. These URL should answer on the `/network/peering` path to report about their identity and endpoints. The endpoint to connect to are collected from the answers of the pods reporting the same currency as ours | `https://g1.data.e-is.pro` |

# Other options

There are many ElasticSearch and Cesium+ pod configuration parameters. Only the most common ones are linked to environment variables. At some point one might want to use a whole custom configuration file.

In this case bind mount this file as `/opt/cesium-pod-plus/config/elasticsearch.yml` and set `CESIUM_CUSTOM_CONFIGURATION_FILE` to `true`:
```
    environment:
      CESIUM_CUSTOM_CONFIGURATION_FILE: "true"
    volumes:
      - /path/to/elasticsearch.yml:/opt/cesium-pod-plus/config/elasticsearch.yml
```

When using such a custom configuration file, all the other `CESIUM_*` environment variables are ignored.
